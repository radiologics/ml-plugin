/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.models.DataSetModelTests
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.models;

import static java.util.Spliterator.ORDERED;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Spliterators;
import java.util.stream.StreamSupport;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.framework.configuration.SerializerConfig;
import org.nrg.framework.services.SerializerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SerializerConfig.class)
public class DataSetModelTests {
    @Test
    public void testRenderedJson() throws IOException {
        final DataSetModel model = createDefaultDataSetModel();
        final String       json  = _serializer.toJson(model);
        assertThat(json).isNotBlank();
        final JsonNode jsonNode = _serializer.deserializeJson(json);
        assertThat(jsonNode).isNotNull();
        final List<String> fieldNames = StreamSupport.stream(Spliterators.spliteratorUnknownSize(jsonNode.fieldNames(), ORDERED), false).collect(toList());
        assertThat(fieldNames).containsAnyElementsOf(EXPECTED);
        assertThat(fieldNames).doesNotContainAnyElementsOf(NOT_EXPECTED);
    }

    private static DataSetModel createDefaultDataSetModel() {
        final DataSetModel model = new DataSetModel();
        model.setLicence("CC-BY-SA 4.0");
        model.setRelease("2.0 04/05/2018");
        model.setTensorImageSize("4D");
        model.setDescription("This is a data collection");
        model.setName("segmentation_mri_prostate_cg_and_pz");
        model.setReference("https://www.med.upenn.edu/sbia/brats2017.html");
        model.getModality().put("0", "FLAIR");
        model.getModality().put("1", "T1w");
        model.getModality().put("2", "t1gd");
        model.getModality().put("3", "T2w");
        model.getLabels().put("0", "background");
        model.getLabels().put("1", "edema");
        model.getLabels().put("2", "non-enhancing tumor");
        model.getLabels().put("3", "enhancing tumour");
        model.getTraining().addAll(TRAINING.stream().map(experiment -> new DataSetExperiment(experiment.getLeft(), experiment.getRight())).collect(toList()));
        model.getValidation().addAll(VALIDATION.stream().map(experiment -> new DataSetExperiment(experiment.getLeft(), experiment.getRight())).collect(toList()));
        model.getTest().addAll(TEST);
        return model;
    }

    private static       List<Pair<String, String>> TRAINING       = Arrays.asList(Pair.of("prostate_00_MR_01/SCANS/1/NIFTI/prostate_00.nii.gz", "prostate_00_MR_01/SCANS/2/NIFTI/prostate_00.nii.gz"),
                                                                                   Pair.of("prostate_01_MR_01/SCANS/1/NIFTI/prostate_01.nii.gz", "prostate_01_MR_01/SCANS/2/NIFTI/prostate_01.nii.gz"),
                                                                                   Pair.of("prostate_02_MR_01/SCANS/1/NIFTI/prostate_02.nii.gz", "prostate_02_MR_01/SCANS/2/NIFTI/prostate_02.nii.gz"),
                                                                                   Pair.of("prostate_04_MR_01/SCANS/1/NIFTI/prostate_04.nii.gz", "prostate_04_MR_01/SCANS/2/NIFTI/prostate_04.nii.gz"),
                                                                                   Pair.of("prostate_06_MR_01/SCANS/1/NIFTI/prostate_06.nii.gz", "prostate_06_MR_01/SCANS/2/NIFTI/prostate_06.nii.gz"),
                                                                                   Pair.of("prostate_07_MR_01/SCANS/1/NIFTI/prostate_07.nii.gz", "prostate_07_MR_01/SCANS/2/NIFTI/prostate_07.nii.gz"),
                                                                                   Pair.of("prostate_10_MR_01/SCANS/1/NIFTI/prostate_10.nii.gz", "prostate_10_MR_01/SCANS/2/NIFTI/prostate_10.nii.gz"),
                                                                                   Pair.of("prostate_13_MR_01/SCANS/1/NIFTI/prostate_13.nii.gz", "prostate_13_MR_01/SCANS/2/NIFTI/prostate_13.nii.gz"),
                                                                                   Pair.of("prostate_14_MR_01/SCANS/1/NIFTI/prostate_14.nii.gz", "prostate_14_MR_01/SCANS/2/NIFTI/prostate_14.nii.gz"),
                                                                                   Pair.of("prostate_16_MR_01/SCANS/1/NIFTI/prostate_16.nii.gz", "prostate_16_MR_01/SCANS/2/NIFTI/prostate_16.nii.gz"),
                                                                                   Pair.of("prostate_17_MR_01/SCANS/1/NIFTI/prostate_17.nii.gz", "prostate_17_MR_01/SCANS/2/NIFTI/prostate_17.nii.gz"),
                                                                                   Pair.of("prostate_18_MR_01/SCANS/1/NIFTI/prostate_18.nii.gz", "prostate_18_MR_01/SCANS/2/NIFTI/prostate_18.nii.gz"),
                                                                                   Pair.of("prostate_20_MR_01/SCANS/1/NIFTI/prostate_20.nii.gz", "prostate_20_MR_01/SCANS/2/NIFTI/prostate_20.nii.gz"),
                                                                                   Pair.of("prostate_21_MR_01/SCANS/1/NIFTI/prostate_21.nii.gz", "prostate_21_MR_01/SCANS/2/NIFTI/prostate_21.nii.gz"),
                                                                                   Pair.of("prostate_24_MR_01/SCANS/1/NIFTI/prostate_24.nii.gz", "prostate_24_MR_01/SCANS/2/NIFTI/prostate_24.nii.gz"),
                                                                                   Pair.of("prostate_25_MR_01/SCANS/1/NIFTI/prostate_25.nii.gz", "prostate_25_MR_01/SCANS/2/NIFTI/prostate_25.nii.gz"),
                                                                                   Pair.of("prostate_28_MR_01/SCANS/1/NIFTI/prostate_28.nii.gz", "prostate_28_MR_01/SCANS/2/NIFTI/prostate_28.nii.gz"),
                                                                                   Pair.of("prostate_29_MR_01/SCANS/1/NIFTI/prostate_29.nii.gz", "prostate_29_MR_01/SCANS/2/NIFTI/prostate_29.nii.gz"),
                                                                                   Pair.of("prostate_31_MR_01/SCANS/1/NIFTI/prostate_31.nii.gz", "prostate_31_MR_01/SCANS/2/NIFTI/prostate_31.nii.gz"),
                                                                                   Pair.of("prostate_32_MR_01/SCANS/1/NIFTI/prostate_32.nii.gz", "prostate_32_MR_01/SCANS/2/NIFTI/prostate_32.nii.gz"),
                                                                                   Pair.of("prostate_34_MR_01/SCANS/1/NIFTI/prostate_34.nii.gz", "prostate_34_MR_01/SCANS/2/NIFTI/prostate_34.nii.gz"),
                                                                                   Pair.of("prostate_35_MR_01/SCANS/1/NIFTI/prostate_35.nii.gz", "prostate_35_MR_01/SCANS/2/NIFTI/prostate_35.nii.gz"),
                                                                                   Pair.of("prostate_37_MR_01/SCANS/1/NIFTI/prostate_37.nii.gz", "prostate_37_MR_01/SCANS/2/NIFTI/prostate_37.nii.gz"),
                                                                                   Pair.of("prostate_38_MR_01/SCANS/1/NIFTI/prostate_38.nii.gz", "prostate_38_MR_01/SCANS/2/NIFTI/prostate_38.nii.gz"),
                                                                                   Pair.of("prostate_39_MR_01/SCANS/1/NIFTI/prostate_39.nii.gz", "prostate_39_MR_01/SCANS/2/NIFTI/prostate_39.nii.gz"),
                                                                                   Pair.of("prostate_40_MR_01/SCANS/1/NIFTI/prostate_40.nii.gz", "prostate_40_MR_01/SCANS/2/NIFTI/prostate_40.nii.gz"));
    private static       List<Pair<String, String>> VALIDATION     = Arrays.asList(Pair.of("prostate_41_MR_01/SCANS/1/NIFTI/prostate_41.nii.gz", "prostate_41_MR_01/SCANS/2/NIFTI/prostate_41.nii.gz"),
                                                                                   Pair.of("prostate_42_MR_01/SCANS/1/NIFTI/prostate_42.nii.gz", "prostate_42_MR_01/SCANS/2/NIFTI/prostate_42.nii.gz"),
                                                                                   Pair.of("prostate_43_MR_01/SCANS/1/NIFTI/prostate_43.nii.gz", "prostate_43_MR_01/SCANS/2/NIFTI/prostate_43.nii.gz"),
                                                                                   Pair.of("prostate_44_MR_01/SCANS/1/NIFTI/prostate_44.nii.gz", "prostate_44_MR_01/SCANS/2/NIFTI/prostate_44.nii.gz"),
                                                                                   Pair.of("prostate_46_MR_01/SCANS/1/NIFTI/prostate_46.nii.gz", "prostate_46_MR_01/SCANS/2/NIFTI/prostate_46.nii.gz"),
                                                                                   Pair.of("prostate_47_MR_01/SCANS/1/NIFTI/prostate_47.nii.gz", "prostate_47_MR_01/SCANS/2/NIFTI/prostate_47.nii.gz"));
    private static       List<String>               TEST           = Arrays.asList("prostate_03_MR_01/SCANS/1/NIFTI/prostate_03.nii.gz",
                                                                                   "prostate_05_MR_01/SCANS/1/NIFTI/prostate_05.nii.gz",
                                                                                   "prostate_08_MR_01/SCANS/1/NIFTI/prostate_08.nii.gz",
                                                                                   "prostate_09_MR_01/SCANS/1/NIFTI/prostate_09.nii.gz",
                                                                                   "prostate_11_MR_01/SCANS/1/NIFTI/prostate_11.nii.gz",
                                                                                   "prostate_12_MR_01/SCANS/1/NIFTI/prostate_12.nii.gz",
                                                                                   "prostate_15_MR_01/SCANS/1/NIFTI/prostate_15.nii.gz",
                                                                                   "prostate_19_MR_01/SCANS/1/NIFTI/prostate_19.nii.gz",
                                                                                   "prostate_22_MR_01/SCANS/1/NIFTI/prostate_22.nii.gz",
                                                                                   "prostate_23_MR_01/SCANS/1/NIFTI/prostate_23.nii.gz",
                                                                                   "prostate_26_MR_01/SCANS/1/NIFTI/prostate_26.nii.gz",
                                                                                   "prostate_27_MR_01/SCANS/1/NIFTI/prostate_27.nii.gz",
                                                                                   "prostate_30_MR_01/SCANS/1/NIFTI/prostate_30.nii.gz",
                                                                                   "prostate_33_MR_01/SCANS/1/NIFTI/prostate_33.nii.gz",
                                                                                   "prostate_36_MR_01/SCANS/1/NIFTI/prostate_36.nii.gz",
                                                                                   "prostate_45_MR_01/SCANS/1/NIFTI/prostate_45.nii.gz");
    private static       List<String>               EXPECTED       = Arrays.asList("description",
                                                                                   "labels",
                                                                                   "licence",
                                                                                   "modality",
                                                                                   "name",
                                                                                   "numTest",
                                                                                   "numTraining",
                                                                                   "reference",
                                                                                   "release",
                                                                                   "tensorImageSize",
                                                                                   "test",
                                                                                   "training",
                                                                                   "validation");
    private static       List<String>               NOT_EXPECTED   = Arrays.asList("testExperiments",
                                                                                   "trainingExperiments",
                                                                                   "validationExperiments");

    @Autowired
    private SerializerService _serializer;
}
