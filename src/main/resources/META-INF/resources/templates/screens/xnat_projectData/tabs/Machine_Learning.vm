#if($data.getSession().getAttribute("userHelper").canDelete("xnat:subjectData/project",$om.id))
<!-- BEGIN META-INF/resources/templates/screens/xnat_projectData/tabs/Machine_Learning.vm -->
<!-- title: Machine Learning -->
<div id="project-ml-panel" class="panel" style="padding: 1em; margin-bottom: 0">
    <h3>Manage Machine Learning Capabilities in this <span style="text-transform:capitalize">$displayManager.getSingularDisplayNameForProject()</span></h3>
    <div class="message">
        Setting up your <span class="lowercase">$displayManager.getSingularDisplayNameForProject()</span> for Machine Learning requires multiple steps. This panel provides a simple summary of the status of your ${displayManager.getSingularDisplayNameForProject()}'s readiness. There are dashboard pages for managing specific components of data curation and model training.  <a href="javascript:void" class="open-ml-help-dialog">Need&nbsp;Help?</a>
    </div>
    <br>
    <div class="panel project-panel">
        <div class="col-2">
            <h3>Datasets</h3>
            #addGlobalCustomScreens("xnat_projectData/datasets")
        </div>
        <div class="col-2">
            <h3>Models and Training</h3>
            <div class="panel-element" data-name="">
                <div class="element-label">Models Installed</div>
                <div class="element-wrapper">
                    <span id="ml-tab-model-count">0</span>
                </div>
                <br class="clear">
            </div>
            <div class="panel-element" data-name="">
                <div class="element-label">Configs Installed</div>
                <div class="element-wrapper">
                    <span id="ml-tab-config-count">0</span>
                </div>
                <br class="clear">
            </div>
            <div class="panel-element" data-name="">
                <div class="element-label">Training Runs</div>
                <div class="element-wrapper"><span id="ml-tab-train-count">0</span></div>
                <br class="clear">
            </div>
            <p>
                <a href="$link.setAction("XDATActionRouter").addPathInfo("xdataction","TrainingDashboard").addPathInfo(
                    "search_element","xnat:projectData").addPathInfo("search_field","xnat:projectData.ID").addPathInfo(
                    "search_value","$!{project.getId()}").addPathInfo("popup","$!popup")">
                    <button class="btn btn-primary">Open Machine Learning Dashboard</button>
                </a>
            </p>
        </div>
        <div class="clearfix clear"></div>
    </div>

</div>

<script>
    console.log('ml-initProject.js');

    (function(factory){
        if (typeof define === 'function' && define.amd) {
            define(factory);
        }
        else if (typeof exports === 'object') {
            module.exports = factory();
        }
        else {
            return factory();
        }
    }(function() {

        var undef;

        var XNAT = getObject(window.XNAT || {});
        XNAT.plugin = getObject(XNAT.plugin || {});
        XNAT.plugin.ml = getObject(XNAT.plugin.ml || {});

        var rootUrl = XNAT.url.rootUrl;

        function showCount(data,type){
            var results = data.ResultSet && data.ResultSet.Result
                    ? data.ResultSet.Result
                    : data;
            if (results.length) {
                $('#ml-tab-'+type+'-count').html(results.length)
            }
        }

        function getItems(url, name) {
            XNAT.xhr.get({
                url: rootUrl(url),
                async: false,
                success: function(data) {
                    showCount(data, name);
                },
                fail: function(e) {
                    console.error(e);
                }
            });
        }

        XNAT.plugin.ml.projectInit = function(projectId) {
            getItems('/data/projects/' + projectId + '/experiments?xsiType=ml:model', "model");
            getItems('/data/projects/' + projectId + '/experiments?xsiType=ml:trainConfig', "config");
            getItems('/data/projects/' + projectId + '/experiments?xsiType=ml:trainSession', "train");
            getItems('/xapi/commands/available?project=' + projectId + '&xsiType=ml:trainSession', "command");
        };

        $(function() {
            XNAT.plugin.ml.projectInit(XNAT.data.context.projectID);
        });

        $(document).on('click','.open-ml-help-dialog',function(e){
            e.preventDefault();
            XNAT.dialog.message({
                width: 550,
                title: 'Help using the Machine Learning control panel',
                content: spawn('!',[
                    spawn('p','This control panel provides a quick snapshot of your training-related data collections ("datasets") and the components of Machine Learning: installed models, training configurations, and experiments. Click the buttons below to manage your datasets or your Machine Learning components.'),
                    spawn('p','For help on any of these functions, see <a href="https://wiki.xnat.org/ml" target="_blank"><b>XNAT ML Documentation</b></a>.')
                ])
            })
        })

    }));

</script>
#else
<!-- if not a project owner, hide the tab added to the project page -->
<script>
    // jq(document).ready(function(){
        jq(document).find('a[href=#Machine_Learning]').parents('li').hide();
    // })
</script>
#end
<!-- END META-INF/resources/templates/screens/xnat_projectData/tabs/Machine_Learning.vm -->
