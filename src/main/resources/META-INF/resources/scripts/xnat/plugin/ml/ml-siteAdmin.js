/*
 * ml-plugin: ml-siteAdmin.js
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

/*!
 * Site-wide Admin UI functions for Machine Learning UI
 */

console.log('ml-siteAdmin.js');

var XNAT = getObject(XNAT || {});
XNAT.plugin = getObject(XNAT.plugin || {});
XNAT.plugin.ml = getObject(XNAT.plugin.ml || {});

(function(factory){
    if (typeof define === 'function' && define.amd) {
        define(factory);
    }
    else if (typeof exports === 'object') {
        module.exports = factory();
    }
    else {
        return factory();
    }
}(function() {

    /* ================ *
     * GLOBAL FUNCTIONS *
     * ================ */

    var undefined,
        rootUrl = XNAT.url.rootUrl,
        restUrl = XNAT.url.restUrl,
        csrfUrl = XNAT.url.csrfUrl;

    function spacer(width) {
        return spawn('i.spacer', {
            style: {
                display: 'inline-block',
                width: width + 'px'
            }
        })
    }

    function errorHandler(e, title, closeAll) {
        console.log(e);
        title = (title) ? 'Error Found: ' + title : 'Error';
        closeAll = (closeAll === undefined) ? true : closeAll;
        var errormsg = (e.statusText) ? '<p><strong>Error ' + e.status + ': ' + e.statusText + '</strong></p><p>' + e.responseText + '</p>' : e;
        XNAT.dialog.open({
            width: 450,
            title: title,
            content: errormsg,
            buttons: [
                {
                    label: 'OK',
                    isDefault: true,
                    close: true,
                    action: function () {
                        if (closeAll) {
                            xmodal.closeAll();

                        }
                    }
                }
            ]
        });
    };

    /* =================== *
     *  DISPLAY ML PREFS   *
     * =================== */

    var prefs;

    XNAT.plugin.ml.prefs = prefs =
        getObject(XNAT.plugin.ml.prefs || {});

    prefs.list = {};

    function unCamelCase(string){
        string = string.replace(/([A-Z])/g, " $1"); // insert a space before all capital letters
        return string.charAt(0).toUpperCase() + string.slice(1); // capitalize the first letter to title case the string
    }

    function prefsUrl(pref){
        var url =  (pref) ? "/xapi/ml/prefs/"+pref : "/xapi/ml/prefs";
        return XNAT.url.rootUrl(url);
    }
    function checkVal(input){
        // if (input.data('value') === undefined) return false;
        // return input.val().toString() === input.data('value').toString();

        // check against the archived prefs.list value
        var key = input.prop('name');
        var val = input.val().toString();
        if (input.prop('type') === "checkbox" && !input.prop('checked')) {
            val = "false";
        }
        return val === XNAT.plugin.ml.prefs.list[key].toString();
    }

    prefs.saveChanges = function(){
        var prefsPanel = $(document).find('div#site-ml-prefs-container'),
            keys = Object.keys(prefs.list),
            error = [],
            success = [],
            updated = false;
        keys.forEach(function(k){
            var input = prefsPanel.find('input[name='+k+']');

            // check to see if current value matches the page's original value
            // only proceed if a user has entered a different value
            if (checkVal(input)) return false;

            updated = true;

            var val = input.val().toString();
            prefs.list[k] = val;
            XNAT.xhr.putJSON({
                url: csrfUrl('/xapi/ml/prefs/'+k),
                data: val,
                fail: function(e){
                    console.warn(e);
                    errorHandler(e,'Could not save preferences');
                },
                success: function(data) {
                    success.push(k);
                    input.data('value',val);
                    XNAT.ui.banner.top(3000,'Saved preferences','success')
                }
            })
        });

        if (!updated) XNAT.ui.banner.top(3000,'No changes to save','message');
        console.log('Saving',prefs.list);
    };

    prefs.panelInit = function(refresh){
        var prefsPanel = $(document).find('div#site-ml-prefs-container');
        prefsPanel.css("margin-top","2em");
        XNAT.xhr.getJSON({
            url: prefsUrl(),
            fail: function(e){ errorHandler(e,"Could not retrieve Machine Learning preferences")},
            success: function(data){
                prefs.list = data;

                // returns an object of key/value pairs
                var keys = Object.keys(data);
                keys.forEach(function(k){
                    if (k === 'tensorBoardEnabled') {
                        prefsPanel.append(spawn('!'),[
                            XNAT.ui.panel.switchbox({
                                name: k,
                                label: 'TensorBoard Enabled',
                                description: 'Enables a live TensorBoard view of a running machine learning experiment. Requires a TensorBoard container and appropriate networking setup. Only set this to true if your setup can support this.',
                                onText: 'Enabled',
                                offText: 'Disabled (Default)',
                                value: 'true',
                                checked: data[k]
                            })
                        ]);
                    } else {
                        prefsPanel.append(spawn('!'),[
                            XNAT.ui.panel.input({
                                name: k,
                                label: unCamelCase(k),
                                value: data[k]
                            })
                        ]);
                    }

                });
            }
        });

        if (!refresh) {
            var footer = $(document).find('#site-ml-prefs-panel').find('.panel-footer');
            footer.append(
                spawn('!',[
                    spawn(
                        'button.btn.primary.pull-right',
                        {onclick: XNAT.plugin.ml.prefs.saveChanges },
                        'Save Changes'
                    ),
                    spawn('div.clearfix.clear')
                ])
            );

        }
    };

    prefs.panelInit();

}));