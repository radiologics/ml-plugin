/*
 * ml-plugin: ml-launchTraining.js
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

console.log('ml-launchTraining.js');

var XNAT = getObject(XNAT || {});
XNAT.plugin = getObject(XNAT.plugin || {});
XNAT.plugin.ml = getObject(XNAT.plugin.ml || {});

(function(factory){
    if (typeof define === 'function' && define.amd) {
        define(factory);
    }
    else if (typeof exports === 'object') {
        module.exports = factory();
    }
    else {
        return factory();
    }
}(function() {

    /* ================ *
     * GLOBAL FUNCTIONS *
     * ================ */

    var undef;
    var projectId = XNAT.data.context.project || getQueryStringValue('project');
    var rootUrl   = XNAT.url.rootUrl;
    var restUrl   = XNAT.url.restUrl;
    var csrfUrl   = XNAT.url.csrfUrl;

    function spacer(width) {
        return spawn('i.spacer', {
            style: {
                display: 'inline-block',
                width: width + 'px'
            }
        })
    }

    function errorHandler(e, title, closeAll) {
        console.log(e);
        title = (title) ? 'Error Found: ' + title : 'Error';
        closeAll = (closeAll === undef) ? true : closeAll;
        var errormsg = (e.statusText) ? '<p><strong>Error ' + e.status + ': ' + e.statusText + '</strong></p><p>' + e.responseText + '</p>' : e;
        XNAT.dialog.open({
            width: 450,
            title: title,
            content: errormsg,
            buttons: [
                {
                    label: 'OK',
                    isDefault: true,
                    close: true,
                    action: function () {
                        if (closeAll) {
                            xmodal.closeAll();
                            XNAT.dialog.closeAll();
                        }
                    }
                }
            ]
        });
    }

    /* =================== *
     * TRAINING RUN CONFIG *
     * =================== */

    var trainingRun
    ;

    XNAT.plugin.ml.trainingRun = trainingRun =
        getObject(XNAT.plugin.ml.trainingRun || {});

    function getConfig(id){
        return XNAT.plugin.ml.projectTrainingConfigs.installedConfigs.filter(function(config){ return config.id === id });
    }

    trainingRun.commands = [];

    trainingRun.openDialog = function(presets){

        presets = firstObject(presets, JSON.parse(presets));

        // check for valid prerequisites for launch
        // var models = XNAT.plugin.ml.projectModels.installedModels || [];
        var modelId = presets.model;
        var configs = XNAT.plugin.ml.projectTrainingConfigs.installedConfigs || [];
        var datasets = XNAT.plugin.datasets.projDatasets.savedDatasets || [];

        if (!configs.length || !datasets.length || !trainingRun.commands.length) {
            errorHandler({
                status: 'ML Component Check',
                statusText: 'Incomplete',
                responseText: "Could not launch training. Please ensure that you have at least one installed model, training config, data collection, and an enabled processing container."
            }, "Could not launch training", true);
            return false;
        }

        function modelInput(modelId){
            return XNAT.ui.panel.input.hidden({
                name: 'model',
                value: modelId
            });
        }

        function modelSelector(models){

            var NAME = 'model';
            var PRESET = presets[NAME] ? presets[NAME] : null;
            var oneItem = models.length === 1;

            var options = [
                ['option', { disabled: 'disabled', selected: !oneItem }, 'Select Model']
            ];

            models.forEach(function(model, i){
                var VALUE = model.id;
                options.push(['option', {
                    value: VALUE,
                    selected: (oneItem && i === 0) || (PRESET === VALUE)
                }, model.label])
            });

            return XNAT.ui.panel.element({
                label: 'Select Model',
                body: spawn('select.required', { name: NAME }, options)
            }).get()

        }

        function configSelector(configs){

            var NAME = 'config';
            var oneItem = configs.length === 1;

            if (!oneItem) {
                var options = [
                    ['option', { value: 'null', selected: true }, 'Select Config']
                ];

                configs.forEach(function(config, i){
                    var VALUE = config.id;
                    options.push(['option', {
                        value: VALUE
                    }, config.label])
                });

            } else {
                var configId = configs[0].id;
                options = [
                    ['option',
                        {
                            value: configId,
                            selected: true
                        }, configs[0].label
                    ]
                ];
                // if only one option exists, we must automatically trigger the reading of launch params from the config
                trainingRun.renderConfigTrainingParams(configId);
            }
            return XNAT.ui.panel.element({
                label: 'Select Training Configuration',
                body: spawn('select.required.train-config-selector', {
                    name: NAME
                }, options)
            }).get()

        }

        function datasetSelector(datasets){

            var NAME = 'collectionId';
            if (!datasets.length) {
                errorHandler({status: 'No Datasets Found'},'Cannot Launch Training',true);
                return false;
            }

            var oneItem = datasets.length === 1;

            if (!oneItem) {
                var options = [
                    ['option', { selected: true, value: 'null' }, 'Select Dataset']
                ];

                datasets.forEach(function(dataset, i){
                    var VALUE = dataset.id;
                    options.push(['option', {
                        value: VALUE
                    }, dataset.label])
                });

            } else {
                var id = datasets[0].id;
                options = [
                    ['option',
                        {
                            value: id,
                            selected: true
                        }, datasets[0].label
                    ]
                ];
            }
            return XNAT.ui.panel.element({
                label: 'Select Dataset',
                body: spawn('select.required.dataset-selector', {
                    name: NAME
                }, options)
            }).get()

        }

        function commandSelector(commands){

            var selector;
            var NAME = 'processingId';
            var PRESET = presets[NAME] ? presets[NAME] : null;
            var oneItem = commands.length === 1;

            var options = [
                ['option', { value: 'null', selected: !oneItem }, 'Select Command']
            ];

            commands.forEach(function(command, i){
                var VALUE = command['wrapper-id'];
                if (command.enabled) {
                    options.push(['option', {
                        value: VALUE,
                        selected: (oneItem && i === 0) || (PRESET === VALUE)
                    }, command['command-name']]);
                }
            });

            if (options.length) {
                selector = spawn('select.train-command-selector', { name: NAME }, options);
            }
            else {
                selector = spawn('div.error', 'No commands available.');
            }

            return XNAT.ui.panel.element({
                label: 'Select Processing Command',
                name: 'processingId',
                body: selector
            }).get();
        }

        function trainingUser(){
            return XNAT.ui.panel.input.hidden({
                name: 'username',
                value: $('#username-link').html()
            });
        }
        function processingId(){
            var d = new Date();
            var timestamp = d.getTime();
            return XNAT.ui.panel.input.hidden({
                name: 'processingId',
                value: timestamp
            });
        }

        XNAT.dialog.open({
            title: 'Launch Training Run',
            width: 600,
            content: spawn('form.trainingRunParams', [
                ['div.panel', [
                    commandSelector(trainingRun.commands),
                    // modelSelector(models),
                    modelInput(modelId),
                    configSelector(configs),
                    datasetSelector(datasets),
                    ['div.selectedDataset.launchParams.hidden'],
                    ['div.datasetParams.launchParams.hidden'],
                    ['div.configParams.launchParams.hidden'],
                    trainingUser(),
                    // processingId()
                ]]
            ]),
            buttons: [
                {
                    label: 'Launch Training',
                    isDefault: true,
                    close: false,
                    action: function(obj){
                        // var model = obj.$modal.find('select[name=model]').find('option:selected').val();
                        var model = obj.$modal.find('input[name=model]').val();
                        var config = obj.$modal.find('select[name=config]').find('option:selected').val(),
                            collection = obj.$modal.find('select[name=collectionId]').find('option:selected').val(),
                            command = obj.$modal.find('select[name=processingId]').find('option:selected').val();

                        if (!model || config === 'null' || collection ==='null' || command === 'null') {
                            XNAT.ui.dialog.message('Please be sure a config, collection, and processing command are selected.');
                            return false;
                        }

                        function createLabel(){
                            return 'training_'+Date.now();
                        }

                        var trainingObj = {
                            configurationId: config,
                            collectionId: collection,
                            label: createLabel(),
                            modelId: model,
                            parameters: {},
                            processingId: command,
                            sessionId: '',
                            user: obj.$modal.find('input[name=username]').val()
                        };

                        obj.$modal.find('.params').each(function(){
                            var key = this.name;
                            trainingObj.parameters[key] = this.value;
                        });

                        xmodal.loading.open({
                            title: 'Preparing to launch training'
                        });

                        XNAT.xhr.postJSON({
                            // method: 'POST',
                            // contentType: 'application/json',
                            // processData: false,
                            url: csrfUrl('/xapi/ml/train/launch'),
                            data: JSON.stringify( trainingObj ),
                            fail: function(e){
                                xmodal.loading.close();
                                if (e.status === 200) {
                                    XNAT.dialog.closeAll();
                                    XNAT.dialog.message({
                                        title: "Training Launched",
                                        content: "New training experiment "+e.responseText+" created."
                                    });
                                    console.log(e);
                                    XNAT.plugin.ml.projectModels.refresh(true);
                                }
                                else {
                                    errorHandler(e, "Could not launch training");
                                    console.log("launchModel: ",trainingObj)
                                }
                            },
                            success: function(data){
                                xmodal.loading.close();
                                XNAT.dialog.closeAll();
                                XNAT.dialog.message({
                                    title: "Training Launched",
                                    content: "New training experiment "+data+" created.",
                                    buttons: [
                                        {
                                            label: 'OK',
                                            isDefault: true,
                                            close: true
                                        }
                                    ]
                                });
                                XNAT.plugin.ml.projectModels.refresh(true);

                            }
                        })
                    }
                },
                // {
                //     label: 'Edit Config',
                //     close: false,
                //     action: function(obj){
                //         var selectedConfig = obj.$modal.find('select[name=config]').find('option:selected');
                //         XNAT.plugin.ml.trainingRun.configEditor(selectedConfig.val(),{ "readonly":false, "label":selectedConfig.html() });
                //     }
                // }
            ]
        });
    };

    trainingRun.renderConfigTrainingParams = function(configId){
        // the training config contains the following launch parameters that must be populated in the launch dialog:
        // - config params ( configuration.parameterizable )
        // - dataset ID ( collectionId )
        // - dataset params ( dataset.parameterizable )

        function populateConfigParams(params){
            var $inputArea = $(document).find('form.trainingRunParams').find('div.configParams');

            // get parameter defaults for this config from the API, rather than hunt and peck through the full configuration template
            XNAT.xhr.getJSON({
                url: rootUrl('/xapi/ml/config/'+configId+'/parameters'),
                success: function(data){
                    var defaults = data;
                    if (params.length){
                        $inputArea.empty();
                        params.forEach(function(param){
                            var label = defaults[param].display,
                                value = defaults[param].default;
                            $inputArea.append(spawn('!',[
                                XNAT.ui.panel.input({
                                    name: param,
                                    label: label,
                                    addClass: 'params',
                                    value: value || ''
                                })
                            ]));
                        });
                    }
                    else {
                        $inputArea.append(spawn('p','No settable parameters for this config.'))
                    }
                    $inputArea.removeClass('hidden');
                },
                fail: function(e){
                    $inputArea.append(spawn('p','Error retrieving default parameters: '+e.status));
                    $inputArea.removeClass('hidden');
                }
            })


        }
        // function populateDatasetId(id){
        //     var $inputArea = $(document).find('form.trainingRunParams').find('div.selectedDataset');
        //     $inputArea.append(spawn('!',[
        //         XNAT.ui.panel.element({
        //             label: 'Selected Dataset',
        //             body: id
        //         }).get(),
        //         XNAT.ui.panel.input.hidden({
        //             name: 'collectionId',
        //             value: id
        //         })
        //     ]));
        //     $inputArea.removeClass('hidden');
        // }
        function populateDatasetParams(params){
            var $inputArea = $(document).find('form.trainingRunParams').find('div.datasetParams');
            $inputArea.empty().append(spawn('!',[
                XNAT.ui.panel.element({
                    style: { 'padding-top:':'1em'},
                    label: 'Dataset Parameters',
                    body: '<pre class=json>'+JSON.stringify(params)+'</pre>'
                }).get()
            ]));
            $inputArea.removeClass('hidden');
        }

        XNAT.xhr.getJSON({
            url: rootUrl('/xapi/ml/config/'+configId),
            success: function(data){
                populateConfigParams(data.configuration.parameterizable);
                // populateDatasetId(data.collectionId);
                populateDatasetParams(data.dataset.parameterizable);
            }
        });
    };


    trainingRun.saveConfig = function(obj){

        // params:

        var configId = obj.id;
        var editorDialog = obj.dialog;  // dialog instance
        var codeEditor = obj.editor;  // editor instance
        var action = obj.action;
        var selectedDataset = obj.selectedDataset;

        var editorContent = codeEditor.getValue().code;
        var annotations   = (codeEditor.aceEditor.getSession().getAnnotations());
        var errors        = [];

        if (annotations.length) {
            errors = annotations.filter(function(item, i){
                return item.type && item.type === 'error'
            });
            if (errors.length) {
                errors = errors.map(function(item, i){
                    return ['li', [
                        ['b', 'line ' + (item.row + 1) + ', column ' + (item.column + 1) + ':'],
                        '<br>',
                        item.text
                    ]]
                });
                XNAT.dialog.message('Error', spawn('div.error', { style: { fontSize: '1em' } }, [
                    (errors.length === 1 ? ' An error was found:' : 'Errors were found:'),
                    ['ul', errors]
                ]));

                // DON'T SAVE if there are code errors
                return false;
            }
        }

        var json = JSON.parse(editorContent);
        // // replace hand-coded collectionId with the value of the select menu
        // json.collectionId = selectedDataset;

        // handle "Edit" vs "New" or "New Copy" behavior
        if (action.toLowerCase() === 'new'){
            //  ask the user to name the new config in a separate dialog, then save

            var configLabelInput = spawn('input.config-label|name=label|type=text|size=25', {
                placeholder: 'Config_Label'
            });

            function submitConfigUrl(label){
                return rootUrl('/xapi/ml/config/project/' + projectId + '/' + label)
            }

            XNAT.dialog.confirm({
                width: 500,
                // height: 400,
                title: 'Save New Config?',
                content: spawn('p', [
                    'Save new config with name: ',
                    configLabelInput
                ]),
                buttons: [
                    {
                        label: 'Save',
                        close: false,
                        isDefault: true,
                        action: function(saveDlg){
                            var label = configLabelInput.value;

                            // DON'T SAVE if there's no label specified
                            if (!label) {
                                XNAT.dialog.message('Error', 'Please enter a label name.');
                                return false;
                            }

                            // Make Config Label conform to ID naming
                            label = label.replace(/([!@#$%^&*\'\"\[\]]|\-|\s)+/g,'_');

                            // TODO: check for naming conflicts with existing config labels before saving

                            console.log(editorContent);

                            XNAT.xhr.postJSON({
                                url: submitConfigUrl(label),
                                data: JSON.stringify(json),
                                dataType: 'text' // the RESPONSE is the new experiment id for the config
                            }).done(function(response, statusText, jqXHR){

                                console.log('done?');
                                console.log(arguments);

                                if (200 <= jqXHR.status < 300) {

                                    XNAT.plugin.ml.projectTrainingConfigs.init();
                                    XNAT.ui.banner.top(2000, 'Training config saved.', 'success');
                                    saveDlg.close();
                                    editorDialog.close();

                                }
                                else {
                                    errorHandler(jqXHR);
                                }


                            }).fail(function(response, statusText, jqXHR){

                                // errorHandler(jqXHR);
                                console.warn(arguments);
                                errorHandler(jqXHR);

                            }).always(function(response, statusText, jqXHR){

                                console.log(arguments);

                            })
                        }
                    }
                ]
            })
        }
        else {
            XNAT.xhr.putJSON({
                url: rootUrl('/xapi/ml/config/'+configId),
                data: JSON.stringify(json),
                processData: false
            }).done(function(response, statusText, jqXHR){

                console.log('done?');
                console.log(arguments);

                if (200 <= jqXHR.status < 300) {

                    XNAT.plugin.ml.projectTrainingConfigs.init();
                    XNAT.ui.banner.top(2000, 'Training config updated.', 'success');
                    saveDlg.close();
                    editorDialog.close();

                }
                else {
                    errorHandler(jqXHR);
                }


            }).fail(function(response, statusText, jqXHR){

                // errorHandler(jqXHR);
                console.warn(arguments);
                errorHandler(jqXHR);

            }).always(function(response, statusText, jqXHR){

                console.log(arguments);

            })
        }

    };


    trainingRun.configEditor = function(cfgId, config, opts){

        var readonly = (opts && opts.readonly) ? opts.readonly : false;
        var cfgLabel = opts.label;

        var _source = spawn('textarea', JSON.stringify(config || {}, null, 4));
        var _editor = XNAT.app.codeEditor.init(_source, {
            language: 'json'
        });

        var datasetOptions = function(preset){
            var datasets = XNAT.plugin.datasets.projDatasets.savedDatasets;
            if (datasets.length) {
                var oneItem = (datasets.length === 1) ? 'selected' : '';
                var options = [['option', { value: '', selected: (!oneItem && !preset) }, 'Select Dataset']];

                datasets.forEach(function(dataset){
                    options.push(['option', {
                        value: dataset.id,
                        selected: (dataset.id === preset)
                    }, dataset.label ])
                });
                return options;
            }
            else {
                return ['option', { disabled: 'disabled', selected: 'selected' }, 'No Datasets To Select'];
            }
        };

        function cfgPreMeta(){
            // if this config editor is being used from the Launch Training dialog,
            // hide all the surrounding meta and only give the user a template editor
            if (opts.templateOnly) {
                return spawn('form.config-metadata', { style: { marginBottom: 0 } },[
                    ['input|type=hidden|name=project', { value: config.project || projectId }],
                    ['input|type=hidden|name=collectionId', { value: config.collectionId }],
                    ['input|type=hidden|name=cfgParameterizable', { value: JSON.stringify(config.configuration.parameterizable) }],
                    ['input|type=hidden|name=datasetTemplate', {value: JSON.stringify(config.dataset.template )}],
                    ['input|type=hidden|name=datasetParams',{ value: JSON.stringify(config.dataset.parameterizable )}],
                    ['table.xnat-table.clean.compact.borderless|style=width:100%', [
                        ['tbody', [
                            ['tr', [
                                ['td|style=width:200px', '<b>Label</b> &nbsp;'],
                                ['td', [ spawn('input.config-label|name=label|size=30') ]]
                            ]]
                        ]]
                    ]]
                ])
            }
            else {
                return spawn('form.config-metadata', { style: { marginBottom: 0 } }, [
                    cfgId && ['input|type=hidden|name=id', { value: cfgId }],
                    ['input|type=hidden|name=project', { value: config.project || projectId }],
                    ['table.xnat-table.clean.compact.borderless|style=width:100%', [
                        ['tbody', [
                            ['tr',[
                                ['td.top|colspan=2', [
                                    ['div.message', [
                                        'The XNAT training configuration object must include several objects:',
                                        ['ol',[
                                            ['li','The model configuration JSON (<b>configuration.template</b>)'],
                                            ['li','Optional user-settable config parameters (<b>configuration.parameterizable</b>)'],
                                            ['li','A set of model-specific JSON that is packaged around the dataset for training (<b>dataset.template</b>)'],
                                            ['li','A set of parameters for how the dataset will be submitted for training (<b>dataset.parameters</b>)']
                                        ]]
                                    ]]
                                ]]
                            ]],
                            ['tr', [
                                ['td.top|colspan=2', '<b>Full Configuration JSON</b> &nbsp;']
                            ]]
                        ]]
                    ]],
                    ''
                ])
            }

        }

        var action = (cfgLabel) ? 'edit':'new';
        var saveLabel = (opts.templateOnly || !cfgLabel) ? 'Save as New Config' : 'Update Config';

        _editor.openEditor({
            title: cfgId ? 'Edit Training Configuration <b>' + opts.label + '</b>' : 'New Training Configuration',
            height: 700,
            classes: 'plugin-json',
            before: cfgPreMeta(),
            buttons: {
                save: {
                    label: saveLabel,
                    isDefault: true,
                    close: false,
                    action: function(dlgObj){
                        trainingRun.saveConfig({
                            id: cfgId,
                            label: cfgLabel,
                            dialog: dlgObj,
                            editor: _editor,
                            action: action
                        })
                    }
                },
                close: {
                    label: 'Close',
                    close: true
                }
            },
            afterShow: function(dialog, obj){
                obj.aceEditor.setReadOnly(readonly);
            }
        });

    };


    trainingRun.editConfig = function(cfgId, opts){
        return XNAT.xhr.getJSON({
            url: XNAT.url.rootUrl('/xapi/ml/config/' + cfgId),
            success: function(data){
                trainingRun.configEditor(cfgId, data, opts)
            },
            failure: function(e){
                errorHandler(e, "Could not retrieve configuration for " + cfgId)
            }
        })
    };

    trainingRun.editConfigTemplate = function(cfgId, opts){
        return XNAT.xhr.getJSON({
            url: XNAT.url.rootUrl('/xapi/ml/config/' + cfgId + '/template'),
            success: function(data){
                trainingRun.configEditor(cfgId, data, opts)
            },
            failure: function(e){
                errorHandler(e, "Could not retrieve configuration for " + cfgId)
            }
        })
    };

    // create a NEW config from scratch
    trainingRun.createConfig = function(opts){
        var defaultConfigObject = {
            project: projectId,
            configuration: {
                parameterizable: ["epochs", "multi_gpu", "learning_rate"],
                template: {}
            },
            dataset: {
                parameterizable: {
                    "training": 70,
                    "validation": 20,
                    "test": 10
                },
                template: {}
            }
        };
        return trainingRun.configEditor('', defaultConfigObject, opts || {});
    };

    trainingRun.setConfigParams = function(event,select){
        var configId = $(select).val();
        var config = getConfig(configId)[0];
        XNAT.form.setValues(select.form, config.params);
    };

    // event listeners

    $(document).on('click','.launch-training',function(){
        var presets = $(this).data("presets");
        trainingRun.openDialog(presets);
    });

    $(document).on('change','.train-config-selector',function(e){
        console.log(e,this);
        var configId = $(this).find('option:selected').val();
        trainingRun.renderConfigTrainingParams(configId);
        // trainingRun.setConfigParams(e,this);
    });

    // init -- find containers that can be run in training
    // requires container service
    trainingRun.init = function () {
        XNAT.xhr.getJSON({
            url: rootUrl('/xapi/commands/available?project=' + projectId + '&xsiType=ml:trainSession'),
            success: function (data) {
                if (data.length) {
                    trainingRun.commands = [].concat(data);
                }
            },
            fail: function (e) {
                console.error(e);
            }
        })
    };

    $(document).ready(function(){
        XNAT.plugin.ml.trainingRun.init();
    })

}));
