/*
 * ml-plugin: ml-trainConfig.js
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

console.log('ml-trainConfig.js');

var XNAT = getObject(XNAT || {});
XNAT.plugin = getObject(XNAT.plugin || {});
XNAT.plugin.ml = getObject(XNAT.plugin.ml || {});

(function(factory){
    if (typeof define === 'function' && define.amd) {
        define(factory);
    }
    else if (typeof exports === 'object') {
        module.exports = factory();
    }
    else {
        return factory();
    }
}(function() {

    /* ================ *
     * GLOBAL FUNCTIONS *
     * ================ */

    var undef;
    var projectId = XNAT.data.context.project || getQueryStringValue('project');
    var rootUrl = XNAT.url.rootUrl;
    var restUrl = XNAT.url.restUrl;
    var csrfUrl = XNAT.url.csrfUrl;
    var xml2json = new X2JS();

    function spacer(width) {
        return spawn('i.spacer', {
            style: {
                display: 'inline-block',
                width: width + 'px'
            }
        })
    }

    function unCamelCase(string) {
        string = string.replace(/([A-Z])/g, " $1"); // insert a space before all capital letters
        return string.charAt(0).toUpperCase() + string.slice(1); // capitalize the first letter to title case the string
    }

    function errorHandler(e, title, closeAll) {
        console.log(e);
        title = (title) ? 'Error Found: ' + title : 'Error';
        closeAll = (closeAll === undef) ? true : closeAll;
        var errormsg = (e.statusText) ? '<p><strong>Error ' + e.status + ': ' + e.statusText + '</strong></p><p>' + e.responseText + '</p>' : e;
        XNAT.dialog.open({
            width: 450,
            title: title,
            content: errormsg,
            buttons: [
                {
                    label: 'OK',
                    isDefault: true,
                    close: true,
                    action: function () {
                        if (closeAll) {
                            xmodal.closeAll();

                        }
                    }
                }
            ]
        });
    }

    /* ========================= *
     * DISPLAY INSTALLED CONFIGS *
     * ========================= */

    var projectTrainingConfigs;

    XNAT.plugin.ml.projectTrainingConfigs = projectTrainingConfigs =
        getObject(XNAT.plugin.ml.projectTrainingConfigs || {});

    projectTrainingConfigs.installedConfigs = [];

    var getConfigsUrl = function () {
        return rootUrl("/xapi/ml/config/project/" + projectId);
    };
    var getConfigUrl = function (id) {
        return rootUrl("/xapi/ml/config/" + id + "/template");
    };
    var getConfigParamsUrl = function (id) {
        return rootUrl("/xapi/ml/config/" + id + "/parameters");
    };
    var getDatasetParamsUrl = function(id){
        return rootUrl("/xapi/ml/config/" + id + "/dataset/parameters")
    };
    var getConfigEditUrl = function(id){
        return XNAT.url.rootUrl('/app/action/XDATActionRouter/xdataction/edit/search_element/ml%3AtrainConfig/search_field/ml%3AtrainConfig.ID/search_value/' + id + '/popup/false/project/' + projectId);
    };

    projectTrainingConfigs.deleteConfig = function(id,label){
        XNAT.ui.dialog.confirm({
            title: 'Delete Config?',
            content: 'Are you sure you want to delete the training config <b>'+label+'</b>? This operation cannot be undone.',
            okAction: function(){
                XNAT.xhr.ajax({
                    url: csrfUrl('/xapi/ml/config/' + id),
                    method: 'DELETE',
                    success: function () {
                        XNAT.ui.banner.top(3000, 'Successfully Deleted Config ' + label, 'success');
                        XNAT.plugin.ml.projectTrainingConfigs.init('refresh');
                    }
                })
            }
        })
    };

    // create data table
    projectTrainingConfigs.table = function (container, callback) {

        // initialize the table - we'll add to it below
        var ptcTable = XNAT.table({
            className: 'project-training-configs xnat-table',
            style: {
                width: '100%',
                marginTop: '15px',
                marginBottom: '15px'
            }
        });

        // add table header row
        ptcTable.thead();
        ptcTable.tr()
            .th({addClass: 'left', html: '<b>Training Config Name</b>'})
            .th('<b>Config Template</b>')
            .th('<b>Config Params</b>')
            .th('<b>Dataset Template</b>')
            .th('<b>Dataset Params</b>')
            .th('<b>Actions</b>');

        // make sure there's a <tbody>
        ptcTable.tbody();

        function editConfigButton(configId) {

            return spawn('button.btn.btn-sm.edit-config-button', {
                html: "<i class='fa fa-pencil' title='Edit Full Config'></i>",
                data: {"configid": configId},
                onclick: function () {
                    window.location.assign(getConfigEditUrl(configId));
                }
            });
        }

        function deleteConfigButton(configId, label) {
            return spawn('button.btn.btn-sm.delete-config-button', {
                html: "<i class='fa fa-trash-o' title='Delete Config'></i>",
                data: {"configid": configId},
                onclick: function () {
                    XNAT.plugin.ml.projectTrainingConfigs.deleteConfig(configId,label)
                }
            })
        }

        function viewConfigTemplate(configId, label) {
            return spawn('button.btn.btn-sm.view-config-template-button', {
                html: "View Config",
                data: {"configid": configId},
                onclick: function () {
                    // XNAT.plugin.ml.mlTrain.editConfigTemplate(configId, {"readonly": true, "label": label})
                    XNAT.xhr.getJSON({
                        url: rootUrl('/xapi/ml/config/project/' + projectId + '/' + configId + '/template'),
                        success: function (data) {
                            XNAT.dialog.open({
                                title: 'View Training Config Template',
                                width: 650,
                                content: '<div id="config-json"></div>',
                                beforeShow: function (obj) {
                                    var container = obj.$modal.find('div#config-json');
                                    // container.empty().append(prettifyJSON(data));
                                    container.empty().append(spawn('pre',JSON.stringify(data,null,4)));
                                },
                                buttons: [
                                    {
                                        label: 'OK',
                                        isDefault: true,
                                        close: true
                                    },
                                    {
                                        label: 'Edit Config',
                                        close: true,
                                        action: function () {
                                            window.location.assign(getConfigEditUrl(configId));
                                            // XNAT.plugin.ml.mlTrain.editConfigTemplate(configId, {
                                            //     "readonly": false,
                                            //     "label": label
                                            // })
                                        }
                                    }
                                ]
                            })
                        }
                    })
                }
            });
        }

        function configLink(configId, label) {
            return spawn('a', {
                href: rootUrl('/data/experiments/' + configId + '?format=html'),
                style: {'font-weight': 'bold'}
            }, label);
        }

        function parseConfigDefaultParams(config, data) {
            // winnow out container params from
            // model config params and store
            // in an object to display
            var params = [];
            var paramsObj = {};

            Object.keys(data).forEach(function (key) {
                paramsObj[key] = data[key];
                paramsObj[key].default = JSON.parse(data[key].default);
                params.push('' +
                    // '<b>' +
                    data[key].display +
                    // '</b>' +
                    // ' (' + key + ')' +
                    '');
            });

            // store config params in context object
            config.params = paramsObj;
            config.paramKeys = params;

            return {
                params: config.params,
                paramKeys: config.paramKeys
            };

        }

        function getConfigDefaultParams(config, success, failure) {
            return XNAT.xhr.getJSON(getConfigParamsUrl(config.id)).done(function (data) {
                if (success && isFunction(success)) {
                    success.apply(this, arguments);
                }
            }).fail(function (e) {
                if (failure && isFunction(failure)) {
                    failure.apply(this, arguments);
                }
                errorHandler(e, 'Could not retrieve params for config ' + config.label + ' for ' + XNAT.app.displayNames.singular.project.toLowerCase() + ' ' + projectId);
            });
        }

        function configDefaultsDialog(id, defaults) {

            var paramOptions = [
                {
                    name: 'epochs',
                    display: 'Epochs',
                    description: ''
                },
                {
                    name: 'learning_rate',
                    display: "Learning Rate",
                    description: ''
                },
                // {
                //     name: 'multi_gpu',
                //     display: "Multi-GPU",
                //     description: ''
                // },
                {
                    name: 'num_training_epoch_per_valid',
                    display: "Validation Frequency in Epochs",
                    description: ''
                },
                {
                    name: 'train_summary_recording_interval',
                    display: "Training Summary Recording Interval",
                    description: ''
                },
                {
                    name: 'use_scanning_window',
                    display: "Use Scanning Window",
                    description: ''
                },
                // {
                //     name: 'train',
                //     display: "!training.attributes.train!",
                //     description: ''
                // },
                // {
                //     name: 'validate',
                //     display: "!training.attributes.validate!",
                //     description: ''
                // },
                null
            ];


            // define the columns here to make sure we have matching <th> and <td> cells
            function tableColumns(param) {
                param = param || {};
                return [
                    {
                        th: ['th.left.text-left', {style: {background: 'none'}}, [
                            ['b', 'Param Name']
                        ]],
                        td: ['td', [
                            ['b', {title: param.name}, param.display]
                        ]]
                    },
                    {
                        th: ['th', {style: {background: 'none'}}, [
                            ['b', 'Default']
                        ]],
                        td: ['td', [
                            ['div.center', {title: param.name}, defaults[param.name] ? defaults[param.name]['default'] : '&ndash;']
                        ]]
                    },
                    {
                        th: ['th', {style: {background: 'none'}}, [
                            ['b', 'Configurable?']
                        ]],
                        // td: ['td.center', [
                        //     XNAT.ui.input.switchbox({
                        //         value: param.name,
                        //         name: param.name,
                        //         title: param.display,
                        //         checked: defaults.hasOwnProperty(param.name),
                        //         // onText: param.onText || 'Configurable',
                        //         // offText: param.offText || 'Not Configurable',
                        //         _____: null
                        //     }).get()
                        // ]]
                        td: ['td.center',defaults.hasOwnProperty(param.name)]
                    }
                ];
            }

            XNAT.dialog.open({
                title: 'Set Configurable Parameters',
                width: 650,
                content: XNAT.ui.panel.init({
                    header: false,
                    border: false,
                    padding: 0,
                    body: spawn('div', [
                        ['div.info', [
                            'Use the interface below to view parameter default values and set which parameters can be modified when launching a training session.'
                        ]],
                        ['br'],
                        ['style', '\n' +
                        'table.extra-pad th, table.extra-pad td { padding: 8px 16px; }\n' +
                        ''],
                        ['table.xnat-table.rows-only.extra-pad', {
                            style: {width: '100%', border: 'none', borderCollapse: 'collapse'}
                        }, [
                            ['thead', {style: {border: '1px solid #aaa', background: '#f0f0f0'}}, [
                                ['tr', tableColumns().map(function (col, i) {
                                    return spawn.apply(null, col.th)
                                })]
                            ]],
                            ['tbody', {style: {border: '1px solid #ccc'}}, paramOptions.map(function (param, i) {
                                return !param ? '' : ['tr', tableColumns(param).map(function (col, i) {
                                    return spawn.apply(null, col.td)
                                })]
                            })]
                        ]]
                    ]),
                    footer: '' ||
                    false //||
                    // [
                    //     ['div.pull-right.float-right.text-right', [
                    //         ['button|type=button.btn.btn1', {
                    //             onclick: function (e) {
                    //                 e.preventDefault();
                    //
                    //             }
                    //         }, 'Save Config Defaults']
                    //     ]],
                    //     ['div.clear.clearfix']
                    // ]
                }).get(),
                buttons: [
                    {
                        label: 'OK',
                        isDefault: true,
                        close: true
                    },
                    {
                        label: 'Edit Config Params',
                        isDefault: false,
                        close: false,
                        action: function(dlg){
                            window.location.assign(getConfigEditUrl(id));
                        }
                    }
                    // {
                    //     label: 'Save',
                    //     isDefault: true,
                    //     close: false,
                    //     action: function (dlg) {
                    //         var setDefaults = dlg.dialog$.find('input:checked').toArray().map(function (input) {
                    //             return input.name
                    //         });
                    //         console.log(setDefaults);
                    //         XNAT.xhr.putJSON({
                    //             url: rootUrl('/xapi/ml/config/project/' + projectId + '/' + id + '/parameters'),
                    //             data: JSON.stringify(setDefaults)
                    //         }).done(function () {
                    //             dlg.close(100);
                    //         });
                    //         // XNAT.dialog.message('Saving...', 'Going to save now...', 'Save', function(msg){
                    //         //     console.log('do the save');
                    //         //     obj.dialog$.fadeOut(2000, function(){
                    //         //         obj.destroy()
                    //         //     });
                    //         // })
                    //     }
                    // },
                    // {
                    //     label: 'Cancel',
                    //     close: true
                    // }
                ]
            })
        }

        function editConfigDefaultParams(config) {
            getConfigDefaultParams(config).done(function (data) {

                var obj = parseConfigDefaultParams(config, data);
                var noDefaults = isEmptyObject(obj.params);


                configDefaultsDialog(config.id, obj.params)

            })
        }

        function viewDatasetTemplate(id) {
            return spawn('button.btn.btn-sm', {
                onclick: function (e) {
                    e.preventDefault();
                    XNAT.xhr.getJSON({
                        url: rootUrl('/xapi/ml/config/project/' + projectId + '/' + id + '/dataset/template'),
                        success: function (data) {
                            XNAT.dialog.open({
                                title: 'View Training Dataset JSON',
                                width: 550,
                                content: '<div id="dataset-json"></div>',
                                beforeShow: function (obj) {
                                    var container = obj.$modal.find('div#dataset-json');
                                    // container.empty().append(prettifyJSON(data));
                                    container.empty().append(spawn('pre',JSON.stringify(data,null,4)));
                                },
                                buttons: [
                                    {
                                        label: 'OK',
                                        isDefault: true,
                                        close: true
                                    }
                                ]
                            })
                        }
                    })
                }
            }, 'View Template');
        }

        function viewDatasetParams(id) {
            return spawn('button.btn.btn-sm', {
                onclick: function (e) {
                    e.preventDefault();
                    XNAT.xhr.getJSON({
                        url: getDatasetParamsUrl(id),
                        success: function (data) {
                            XNAT.dialog.open({
                                title: 'View Dataset Parameters',
                                width: 550,
                                content: '<div id="param-json"></div>',
                                beforeShow: function (obj) {
                                    var container = obj.$modal.find('div#param-json');
                                    // container.append(prettifyJSON(data));
                                    container.append(spawn('pre',JSON.stringify(data,null,4)));
                                },
                                buttons: [
                                    {
                                        label: 'OK',
                                        isDefault: true,
                                        close: true
                                    }
                                ]
                            })
                        }
                    })
                }
            }, 'View Params');
        }

        // get project configs
        XNAT.xhr.getJSON({
            url: getConfigsUrl(),
            success: function (data) {

                var configIndex = Object.keys(data).sort();

                projectTrainingConfigs.installedConfigs = configIndex.map(function (k, i) {
                    return {"id": k, "label": data[k], "params": {}};
                });

                if (projectTrainingConfigs.installedConfigs.length) {
                    projectTrainingConfigs.installedConfigs.forEach(function (config) {

                        var cfgParams = spawn('div.config-params.center', [
                            ['button.enable-params.btn.btn-sm', {
                                title: config.id + ': Defaults',
                                html: 'View Params',
                                // data: {
                                //     params: obj.paramKeys.join(',')
                                // },
                                onclick: function (e) {
                                    e.preventDefault();
                                    editConfigDefaultParams(config)
                                }
                            }]
                        ]);

                        ptcTable
                            .tr({
                                // id: config.id,
                                addClass: 'highlight',
                                data: {
                                    'id': config.id,
                                    'config': config.label
                                }
                            })
                            .td([configLink(config.id, config.label)])
                            .td([['div.center', [viewConfigTemplate(config.id, config.label)]]])
                            .td([['div.center', [cfgParams]]])
                            .td([['div.center', [viewDatasetTemplate(config.id)]]])
                            .td([['div.center', [viewDatasetParams(config.id)]]])
                            .td([['div.center', [editConfigButton(config.id, config.label), spacer(6), deleteConfigButton(config.id, config.label)]]]);

                        // get the config params and render them to the cell
                        // renderConfigTrainingParams(config, $(cfgParamsContainer))

                    });
                }
                else {
                    ptcTable.tr()
                        .td({colSpan: 6, html: 'No training configurations have been installed in this '+XNAT.app.displayNames.singular.project.toLowerCase()+'.'});
                }
                if (container) {
                    $$(container).append(ptcTable.table);
                }

                if (isFunction(callback)) {
                    callback(ptcTable.table);
                }
            },
            failure: function (e) {
                errorHandler(e, "Could not retrieve models for " + projectId)
            }
        })
    };

    projectTrainingConfigs.init = function (refresh) {

        var $configList = $('div#proj-training-config-list-container').empty();

        projectTrainingConfigs.table($configList);

        if (!refresh){
            // add help button to header
            var $header = $configList.closest('.panel').find('.panel-heading');
            var helpButton = spawn('button.btn.btn-hover.sm.pull-right', {
                style: {color: '#fff'},
                onclick: function(e){
                    e.preventDefault();

                    XNAT.dialog.message({
                        width: 550,
                        title: 'Help Installing and Managing Training Configs',
                        content: spawn('!',[
                            spawn('p','An XNAT ML Training Config contains configuration JSON as well as several other properties, which can be entered via the XNAT UI.'),
                            spawn('p','For help creating a working configuration, see <a href="https://wiki.xnat.org/ml/creating-a-training-config-for-your-model" target="_blank"><b>XNAT ML Documentation</b></a>')
                        ])
                    })
                }
            },[ spawn('i.fa.fa-info-circle') ]);
            $header.prepend(helpButton);

            var $footer = $configList.closest('.panel').find('.panel-footer').empty();

            // add the 'add new' button to the panel footer
            $footer.append(spawn('!', [
                ['div.pull-right', [
                    ['button.new-config.btn.btn-sm.submit', {
                        html: 'New Training Config',
                        onclick: function () {
                            // XNAT.plugin.ml.mlTrain.createConfig();
                            window.location.assign(XNAT.url.rootUrl('/app/template/XDATScreen_edit_ml_trainConfig.vm/project/' + projectId));
                        }
                    }]
                ]],
                ['div.clear.clearfix']
            ]));
        }


    };

    try {
        projectTrainingConfigs.init();
        // projectTrainingConfigs.showParams();
    } catch (e) {
        errorHandler(e);
    }

}));