/*
 * ml-plugin: org.nrg.xdat.om.base.BaseMlModel
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

/*
 * GENERATED FILE
 * Created on Mon Oct 21 21:40:20 CDT 2019
 *
 */

package org.nrg.xdat.om.base;

import org.apache.commons.lang3.StringUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.MlModelI;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.base.auto.AutoMlModel;
import org.nrg.xft.ItemI;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.ml.exceptions.XnatMlException;
import org.springframework.core.io.InputStreamSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import javax.annotation.Nullable;
import java.nio.file.Path;
import java.util.Hashtable;
import java.util.Map;

/**
 * Override of generated implementation of this class to provide JSON conversion methods.
 **/
@SuppressWarnings("unused")
public abstract class BaseMlModel extends AutoMlModel {
    public static final String MODEL_RESOURCE_NAME    = "Model";
    public static final String MODEL_RESOURCE_FORMAT  = "Data";
    public static final String MODEL_RESOURCE_CONTENT = "Starting point for model training";
    public static final String STATUS_NEW             = "New";
    public static final String STATUS_TRAINING        = "Training";
    public static final String STATUS_PUBLISHED       = "Published";

    public BaseMlModel(final ItemI item) {
        super(item);
        _helper = new MlResourceHelper<>(this);
    }

    public BaseMlModel(final UserI user) {
        super(user);
        _helper = new MlResourceHelper<>(this);
    }

    /**
     * @deprecated Use {@link #BaseMlModel(UserI user)).
     */
    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    public BaseMlModel() {
        _helper = new MlResourceHelper<>(this);
    }

    public BaseMlModel(final Hashtable properties, final UserI user) {
        super(properties, user);
        _helper = new MlResourceHelper<>(this);
    }

    public static boolean validateModelId(final String modelId) {
        return XDAT.getNamedParameterJdbcTemplate().queryForObject(QUERY_VERIFY_MODEL_ID, new MapSqlParameterSource("modelId", modelId), Boolean.class);
    }

    @Override
    public void preSave() {
        if (StringUtils.isBlank(getStatus())) {
            setStatus(STATUS_NEW);
        }
    }

    /**
     * Gets the {@link XnatResourcecatalog resource catalog} for this model. If the catalog has not
     * yet been created (i.e. no actual model data is associated with this object, this returns
     * <b>null</b>. If you want to create a resource catalog if it doesn't yet exist, call the
     * method {@link #getModelResource(UserI)} instead.
     *
     * @return The resource catalog for this model, <b>null</b> if it doesn't have one.
     */
    @Nullable
    public XnatResourcecatalog getModelResource() {
        return getModelResource(null);
    }

    /**
     * Gets the resource catalog for this model. If the catalog has not yet been created, a new
     * catalog is created (<b>user</b> must have sufficient permissions) and returned.
     *
     * @return The resource catalog for this model.
     */
    @Nullable
    public XnatResourcecatalog getModelResource(final @Nullable UserI user) {
        return _helper.getXnatMlResource(user, MODEL_RESOURCE_NAME);
    }

    public Path getModelData() {
        return _helper.getXnatMlResourceData();
    }

    /**
     * Adds the file or files indicated by the <b>paths</b> parameter to the resource catalog.
     *
     * @param user       The user adding data to the resource.
     * @param references One or more URIs indicating the data to add to the resource.
     *
     * @return The created or updated resource catalog.
     *
     * @throws XnatMlException When an error occurs.
     */
    public XnatResourcecatalog setModelData(final UserI user, final Map<String, ? extends InputStreamSource> references) throws XnatMlException {
        return _helper.setXnatMlResourceData(user, MODEL_RESOURCE_NAME, MODEL_RESOURCE_FORMAT, MODEL_RESOURCE_CONTENT, references);
    }

    private static final String QUERY_VERIFY_MODEL_ID = "SELECT EXISTS(SELECT x.id FROM ml_model m LEFT JOIN xnat_experimentdata x ON m.id = x.id WHERE m.id = :modelId)";

    private final MlResourceHelper<MlModelI> _helper;
}
