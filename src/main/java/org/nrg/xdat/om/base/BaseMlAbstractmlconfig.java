/*
 * ml-plugin: org.nrg.xdat.om.base.BaseMlAbstractmlconfig
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

/*
 * GENERATED FILE
 * Created on Mon Oct 21 21:40:20 CDT 2019
 *
 */

package org.nrg.xdat.om.base;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.Hashtable;
import java.util.List;
import org.apache.commons.lang3.ObjectUtils;
import org.nrg.xdat.model.MlParameterizedconfigI;
import org.nrg.xdat.om.MlParameterizedconfig;
import org.nrg.xdat.om.base.auto.AutoMlAbstractmlconfig;
import org.nrg.xft.ItemI;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.ml.exceptions.XnatMlException;

/**
 * Override of generated implementation of this class to provide JSON conversion methods.
 **/
public abstract class BaseMlAbstractmlconfig extends AutoMlAbstractmlconfig {
    public BaseMlAbstractmlconfig(final ItemI item) {
        super(item);
    }

    public BaseMlAbstractmlconfig(final UserI user) {
        super(user);
    }

    /**
     * @deprecated Use {@link #BaseMlAbstractmlconfig(UserI user)}.
     */
    @Deprecated
    public BaseMlAbstractmlconfig() {
    }

    public BaseMlAbstractmlconfig(final Hashtable properties, final UserI user) {
        super(properties, user);
    }

    public void setConfiguration(final String template, final List<String> parameterizable) {
        final MlParameterizedconfig configuration = new MlParameterizedconfig();
        configuration.setTemplate(template);
        configuration.setParameterPaths(parameterizable);
        try {
            setConfiguration((MlParameterizedconfigI) configuration);
        } catch (Exception e) {
            throw new XnatMlException(e);
        }
    }

    @SuppressWarnings("unused")
    public void setConfigurationFromJson(final JsonNode node) {
        final MlParameterizedconfig configuration = ObjectUtils.defaultIfNull(getConfiguration(), new MlParameterizedconfig());
        configuration.setFromJson(node);
        try {
            setConfiguration((MlParameterizedconfigI) configuration);
        } catch (Exception e) {
            throw new XnatMlException(e);
        }
    }
}
