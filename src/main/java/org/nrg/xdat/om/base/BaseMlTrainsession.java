/*
 * ml-plugin: org.nrg.xdat.om.base.BaseMlTrainsession
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xdat.om.base;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import javax.annotation.Nullable;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.framework.services.SerializerService;
import org.nrg.xapi.exceptions.DataFormatException;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.MlTrainsessionI;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.MlModel;
import org.nrg.xdat.om.MlTrainconfig;
import org.nrg.xdat.om.SetsCollection;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.base.auto.AutoMlTrainsession;
import org.nrg.xft.ItemI;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.archive.CatalogService;
import org.nrg.xnatx.plugins.ml.exceptions.XnatMlException;
import org.springframework.core.io.InputStreamSource;

/**
 * Override of generated implementation of this class to provide JSON conversion methods.
 **/
@Slf4j
@SuppressWarnings({"DeprecatedIsStillUsed", "unused"})
public abstract class BaseMlTrainsession extends AutoMlTrainsession {
    public static final String       SESSION_FORMAT                 = "XNAT ML Training Session";
    public static final String       STATUS_IN_PROGRESS             = "in_progress";
    public static final String       CONFIGURATION_RESOURCE_NAME    = "Configuration";
    public static final String       CONFIGURATION_RESOURCE_FORMAT  = "JSON";
    public static final String       CONFIGURATION_RESOURCE_CONTENT = "Training and data configuration files";
    public static final String       RESULTS_RESOURCE_NAME          = "Results";
    public static final String       RESULTS_RESOURCE_FORMAT        = "Data";
    public static final String       RESULTS_RESOURCE_CONTENT       = "Final model checkpoints and other generated artifacts";

    public BaseMlTrainsession(final ItemI item) {
        super(item);
        _helper = new MlResourceHelper<>(this);
        _serializer = XDAT.getSerializerService();
    }

    public BaseMlTrainsession(final UserI user) {
        super(user);
        _helper = new MlResourceHelper<>(this);
        _serializer = XDAT.getSerializerService();
    }

    public BaseMlTrainsession(final Hashtable properties, final UserI user) {
        super(properties, user);
        _helper = new MlResourceHelper<>(this);
        _serializer = XDAT.getSerializerService();
    }

    /**
     * @deprecated Use {@link #BaseMlTrainsession(UserI)}.
     */
    @Deprecated
    public BaseMlTrainsession() {
        _helper = new MlResourceHelper<>(this);
        _serializer = XDAT.getSerializerService();
    }

    public void preSave() throws Exception {
        super.preSave();
        final String trainconfigId = getTrainconfigId();
        if (StringUtils.isNotBlank(trainconfigId) && !MlTrainconfig.validateTrainconfigId(trainconfigId)) {
            throw new DataFormatException("Training sessions must be saved with either a valid training configuration ID or a blank value for that field: " + trainconfigId + " is neither blank nor a valid training configuration ID");
        }
        final String modelId = getModelId();
        if (StringUtils.isNotBlank(modelId) && !MlModel.validateModelId(modelId)) {
            throw new DataFormatException("Training sessions must be saved with either a valid model ID or a blank value for that field: " + modelId + " is neither blank nor a valid model ID");
        }
        final String collectionId = getCollectionId();
        if (StringUtils.isNotBlank(collectionId) && !SetsCollection.validateCollectionId(collectionId)) {
            throw new DataFormatException("Training sessions must be saved with either a valid collection ID or a blank value for that field: " + collectionId + " is neither blank nor a valid collection ID");
        }
    }

    /**
     * Gets the parameters for the training session as a map of strings, where the map key is a
     * JSON path to an item in the training configuration template and the map value is the value to
     * be set for the item.
     *
     * @return A map of the parameters for the training session.
     */
    public Map<String, String> getParameterMap() {
        try {
            final String parameters = getParameters();
            return StringUtils.isBlank(parameters) ? Collections.emptyMap() : _serializer.deserializeJsonToMapOfStrings(parameters);
        } catch (IOException e) {
            log.warn("An error occurred trying to deserialize a JSON parameter map", e);
            return Collections.emptyMap();
        }
    }

    /**
     * Sets the parameters for the training session from a map of strings, where the map key is a
     * JSON path to an item in the training configuration template and the map value is the value to
     * be set for the item.
     *
     * @param parameters The parameters for the training session.
     */
    public void setParameterMap(final Map<String, String> parameters) {
        try {
            setParameters(_serializer.toJson(parameters == null || parameters.isEmpty() ? Collections.emptyMap() : parameters));
        } catch (IOException e) {
            log.warn("An error occurred trying to serialize a parameter map to JSON", e);
        }
    }

    /**
     * Gets the resource catalog for this model. If the catalog has not yet been created, a new
     * catalog is created (<b>user</b> must have sufficient permissions) and returned.
     *
     * @return The resource catalog for this model.
     */
    @Nullable
    public XnatResourcecatalog getSessionResource(final @Nullable UserI user, final String name, final String format, final String content) {
        return _helper.getXnatMlResource(user, name, format, content);
    }

    public Path getSessionData() {
        return _helper.getXnatMlResourceData();
    }

    /**
     * Adds the file or files indicated by the <b>paths</b> parameter to the resource catalog.
     *
     * @param user       The user adding data to the resource.
     * @param references One or more URIs indicating the data to add to the resource.
     *
     * @return The created or updated resource catalog.
     *
     * @throws XnatMlException When an error occurs.
     */
    public XnatResourcecatalog setSessionData(final UserI user, final Map<String, InputStreamSource> references) throws XnatMlException {
        return _helper.setXnatMlResourceData(user, CONFIGURATION_RESOURCE_NAME, CONFIGURATION_RESOURCE_FORMAT, CONFIGURATION_RESOURCE_CONTENT, references);
    }

    public List<XnatAbstractresourceI> getSessionResults() {
        return _helper.getResources();
    }

    /**
     * Creates the session results resource catalog.
     *
     * @param user The user adding data to the resource.
     *
     * @return The created resource catalog.
     *
     * @throws XnatMlException When an error occurs.
     */
    public XnatResourcecatalog createSessionResults(final UserI user) throws XnatMlException {
        return _helper.createXnatMlResource(user, RESULTS_RESOURCE_NAME, RESULTS_RESOURCE_FORMAT, RESULTS_RESOURCE_CONTENT);
    }

    /**
     * Adds the file or files indicated by the <b>paths</b> parameter to the resource catalog.
     *
     * @param user       The user adding data to the resource.
     * @param references One or more URIs indicating the data to add to the resource.
     *
     * @return The created or updated resource catalog.
     *
     * @throws XnatMlException When an error occurs.
     */
    public XnatResourcecatalog setSessionResults(final UserI user, final String name, final String format, final String content, final Map<String, InputStreamSource> references) throws XnatMlException {
        return _helper.setXnatMlResourceData(user, RESULTS_RESOURCE_NAME, RESULTS_RESOURCE_FORMAT, RESULTS_RESOURCE_CONTENT, references);
    }

    private void refreshCatalog(final UserI user, final String resource) {
        try {
            getCatalogService().refreshResourceCatalog(user, resource);
        } catch (ServerException | ClientException e) {
            log.warn("An error occurred when user {} tried to refresh the \"{}\" resource catalog for training session {}", user.getUsername(), resource, getId());
        }
    }

    private CatalogService getCatalogService() {
        if (_catalogService == null) {
            _catalogService = XDAT.getContextService().getBean(CatalogService.class);
        }
        return _catalogService;
    }

    private final MlResourceHelper<MlTrainsessionI> _helper;
    private final SerializerService                 _serializer;

    private CatalogService _catalogService;
}
