/*
 * ml-plugin: org.nrg.xdat.om.base.BaseMlTrainconfig
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xdat.om.base;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.Hashtable;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.MlParameterizedconfigI;
import org.nrg.xdat.om.MlParameterizedconfig;
import org.nrg.xdat.om.base.auto.AutoMlTrainconfig;
import org.nrg.xft.ItemI;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.ml.exceptions.XnatMlException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

/**
 * Override of generated implementation of this class to provide JSON conversion methods.
 **/
@Slf4j
@SuppressWarnings({"DeprecatedIsStillUsed", "unused"})
public abstract class BaseMlTrainconfig extends AutoMlTrainconfig {
    public BaseMlTrainconfig(final ItemI item) {
        super(item);
    }

    public BaseMlTrainconfig(final UserI user) {
        super(user);
    }

    public BaseMlTrainconfig(final Hashtable properties, final UserI user) {
        super(properties, user);
    }

    /**
     * @deprecated Use {@link #BaseMlTrainconfig(UserI)}.
     */
    @Deprecated
    public BaseMlTrainconfig() {
    }

    public static boolean validateTrainconfigId(final String trainconfigId) {
        return XDAT.getNamedParameterJdbcTemplate().queryForObject(QUERY_VERIFY_TRAINCONFIG_ID, new MapSqlParameterSource("trainconfigId", trainconfigId), Boolean.class);
    }

    public void setDataset(final String template, final List<String> parameterizable) {
        final MlParameterizedconfig configuration = new MlParameterizedconfig();
        configuration.setTemplate(template);
        configuration.setParameterPaths(parameterizable);
        try {
            setDataset((MlParameterizedconfigI) configuration);
        } catch (Exception e) {
            throw new XnatMlException(e);
        }
    }

    public void setDatasetFromJson(final JsonNode node) {
        final MlParameterizedconfig configuration = ObjectUtils.defaultIfNull(getConfiguration(), new MlParameterizedconfig());
        configuration.setFromJson(node);
        try {
            setDataset((MlParameterizedconfigI) configuration);
        } catch (Exception e) {
            throw new XnatMlException(e);
        }
    }

    private static final String QUERY_VERIFY_TRAINCONFIG_ID = "SELECT EXISTS(SELECT x.id FROM ml_trainconfig s LEFT JOIN xnat_experimentdata x ON s.id = x.id WHERE s.id = :trainconfigId)";
}
