/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.services.impl.DefaultMlModelService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.services.impl;

import static lombok.AccessLevel.PRIVATE;

import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.nrg.framework.services.SerializerService;
import org.nrg.xapi.exceptions.InsufficientPrivilegesException;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xdat.om.MlModel;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.services.cache.UserDataCache;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnatx.plugins.ml.exceptions.XnatMlException;
import org.nrg.xnatx.plugins.ml.preferences.MlResources;
import org.nrg.xnatx.plugins.ml.services.MlModelService;
import org.springframework.core.io.InputStreamSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

// TODO: Migrate to extend AbstractXftDatasetObjectService instead of AbstractManagementService

/**
 * Manages XNAT ML data objects and operations.
 */
@Service
@Getter(PRIVATE)
@Accessors(prefix = "_")
@Slf4j
public class DefaultMlModelService extends AbstractManagementService<MlModel> implements MlModelService {
    public DefaultMlModelService(final SerializerService serializer, final UserDataCache cache, final NamedParameterJdbcTemplate template, final MlResources resources) {
        super(template, resources);
        _serializer = serializer;
        _cache = cache;
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlModel createMlModel(final UserI user, final MlModel model) {
        model.setStatus(MlModel.STATUS_NEW);
        return saveExperiment(user, model);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlModel createMlModel(final UserI user, final String project, final String label) {
        return createMlModel(user, populateNewMlModel(user, project, label));
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlModel createMlModel(final UserI user, final MlModel model, final Map<String, ? extends InputStreamSource> references) {
        final MlModel          created = createMlModel(user, model);
        final XnatResourcecatalog catalog = created.setModelData(user, references);
        log.info("Created new ML data model object {} along with model resources in catalog {}: {}", created.getId(), catalog.getUri(), StringUtils.join(references.keySet(), ", "));
        try {
            return retrieveMlModel(user, created.getId());
        } catch (NotFoundException e) {
            throw new XnatMlException(e, "Just created a new ML data model with ID " + created.getId() + " but now I'm getting not found. Weird.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlModel createMlModel(final UserI user, final String project, final String label, final Map<String, ? extends InputStreamSource> references) {
        return createMlModel(user, populateNewMlModel(user, project, label), references);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Map<String, String> retrieveMlModels(final UserI user, final String project) throws NotFoundException, InsufficientPrivilegesException {
        // TODO: No permissions checks here
        return getObjectsInProject(user, project);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlModel retrieveMlModel(final UserI user, final String modelId) throws NotFoundException {
        return getExperiment(user, modelId);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlModel retrieveMlModel(@Nonnull final UserI user, @Nullable final String project, @Nonnull final String idOrLabel) throws NotFoundException {
        return retrieveMlModel(user, resolveIdOrLabelInProject(project, idOrLabel));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nonnull
    public MlModel updateMlModel(final UserI user, final MlModel model) {
        // This looks weird, but there's really little difference between create and update operations.
        return createMlModel(user, model);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlModel updateMlModel(final UserI user, final MlModel model, final Map<String, ? extends InputStreamSource> references) {
        final MlModel          updated = saveExperiment(user, model);
        final XnatResourcecatalog catalog = updated.setModelData(user, references);
        log.info("Updated ML data model object {} along with model resources in catalog {}: {}", updated.getId(), catalog.getUri(), StringUtils.join(references.keySet(), ", "));
        try {
            return retrieveMlModel(user, updated.getId());
        } catch (NotFoundException e) {
            throw new XnatMlException(e, "Just updated an existing ML data model with ID " + updated.getId() + " but now I'm getting not found. Weird.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlModel updateMlModel(final UserI user, final String modelId, final Map<String, ? extends InputStreamSource> references) throws NotFoundException {
        return updateMlModel(user, getExperiment(user, modelId), references);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlModel updateMlModel(final UserI user, final String project, final String label, final Map<String, ? extends InputStreamSource> references) throws NotFoundException {
        return updateMlModel(user, getExperiment(user, resolveIdOrLabelInProject(project, label)), references);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteMlModel(final UserI user, final String modelId) throws NotFoundException {
        deleteMlModel(user, null, modelId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteMlModel(final UserI user, final String project, final String idOrLabel) throws NotFoundException {
        final MlModel config = retrieveMlModel(user, project, idOrLabel);
        try {
            SaveItemHelper.authorizedDelete(config.getItem(), user, EventUtils.DEFAULT_EVENT(user, "Deleted ML data model for project " + project));
        } catch (Exception e) {
            throw new XnatMlException(user, e, getMessage("error.unknown", user.getUsername() + " deleting ML data model " + idOrLabel + " in project " + project));
        }
    }

    @Nonnull
    private MlModel populateNewMlModel(final UserI user, final String project, final String label) {
        final MlModel model = new MlModel();
        model.setId(getNewExperimentId());
        model.setUser(user);
        model.setProject(project);
        model.setLabel(label);
        return model;
    }

    private final SerializerService _serializer;
    private final UserDataCache     _cache;
}
