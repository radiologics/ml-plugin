/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.services.MlProcessingService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.services;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import javax.annotation.Nonnull;
import org.nrg.xapi.exceptions.DataFormatException;
import org.nrg.xapi.exceptions.InsufficientPrivilegesException;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.exceptions.ResourceAlreadyExistsException;
import org.nrg.xdat.model.MlModelI;
import org.nrg.xdat.model.MlTrainconfigI;
import org.nrg.xdat.model.SetsCollectionI;
import org.nrg.xdat.om.MlModel;
import org.nrg.xdat.om.MlTrainconfig;
import org.nrg.xdat.om.MlTrainsession;
import org.nrg.xdat.om.SetsCollection;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.ml.models.LaunchTrainingRequestData;

@SuppressWarnings("unused")
public interface MlProcessingService {
    /**
     * Launches a session of the specified {@link MlTrainconfig training configuration}.
     *
     * @param user        The user requesting the training session launch.
     * @param launchModel Contains info required to launch training session.
     *
     * @return The newly created training session object.
     */
    @Nonnull
    MlTrainsession launchTrainingSession(final UserI user, final LaunchTrainingRequestData launchModel) throws NotFoundException, ResourceAlreadyExistsException, DataFormatException;

    /**
     * Creates and launches a training session with the specified {@link MlModel model}, {@link MlTrainconfig training
     * configuration}, and {@link SetsCollection dataset}.
     *
     * @param user          The user requesting the training session launch.
     * @param processingId  The ID for the processing entry to handle the training session.
     * @param label         A label for this particular training session.
     * @param model         The model to use with the launch.
     * @param configuration The configuration to use with the launch.
     * @param collection    The dataset to use with the launch.
     * @param parameters    The parameters for the training session.
     *
     * @return The newly created training session object.
     */
    @Nonnull
    MlTrainsession launchTrainingSession(final UserI user, final String processingId, final String label, final MlModelI model, final MlTrainconfigI configuration, final SetsCollection collection, final Map<String, String> parameters) throws ResourceAlreadyExistsException, DataFormatException;

    /**
     * Creates and launches a training session with the specified {@link MlModel model}, {@link MlTrainconfig training
     * configuration}, and {@link SetsCollection dataset}.
     *
     * @param user            The user requesting the training session launch.
     * @param processingId    The ID for the processing entry to handle the training session.
     * @param label           A label for this particular training session.
     * @param modelId         The ID of the model to use with the launch.
     * @param configurationId The ID of the configuration to use with the launch.
     * @param collectionId    The ID of the dataset to use with the launch.
     * @param parameters      The parameters for the training session.
     *
     * @return The newly created training session object.
     */
    @Nonnull
    MlTrainsession launchTrainingSession(final UserI user, final String processingId, final String label, final String modelId, final String configurationId, final String collectionId, final Map<String, String> parameters) throws NotFoundException, ResourceAlreadyExistsException;

    /**
     * Creates and launches a training session with the specified {@link MlModel model}, {@link MlTrainconfig training
     * configuration}, and {@link SetsCollection dataset}. The newly created training session is labeled with an auto-generated
     * label based on the IDs of the model, configuration, and the timestamp of the training session launch.
     *
     * @param user            The user requesting the training session launch.
     * @param processingId    The ID for the processing entry to handle the training session.
     * @param modelId         The ID of the model to use with the launch.
     * @param configurationId The ID of the configuration to use with the launch.
     * @param collectionId    The ID of the dataset to use with the launch.
     * @param parameters      The parameters for the training session.
     *
     * @return The newly created training session object.
     */
    @Nonnull
    MlTrainsession launchTrainingSession(final UserI user, final String processingId, final String modelId, final String configurationId, final String collectionId, final Map<String, String> parameters) throws NotFoundException, ResourceAlreadyExistsException;

    /**
     * Queues the training session for execution.
     *
     * @param user      The user requesting training session execution.
     * @param sessionId The ID of the training session object to be queued.
     *
     * @return The updated training session object.
     */
    MlTrainsession queueTrainingSession(final UserI user, final String sessionId) throws NotFoundException, DataFormatException;

    /**
     * Queues the training session for execution.
     *
     * @param user      The user requesting training session execution.
     * @param project   The ID of the project containing the training session object to be queued.
     * @param idOrLabel The ID or label of the training session within the specified project.
     *
     * @return The updated training session object.
     */
    MlTrainsession queueTrainingSession(final UserI user, final String project, final String idOrLabel) throws NotFoundException, DataFormatException;

    /**
     * Queues the training session for execution.
     *
     * @param user    The user requesting training session execution.
     * @param session The training session object to be queued.
     *
     * @return The updated training session object.
     */
    MlTrainsession queueTrainingSession(final UserI user, final MlTrainsession session) throws DataFormatException;

    /**
     * Gets the annotation of the training session indicated by the ID.
     *
     * @param user      The user requesting the training session annotation.
     * @param sessionId The ID of the training session.
     *
     * @return The annotation of the specified training session.
     */
    @Nonnull
    String getSessionAnnotation(final UserI user, final String sessionId) throws NotFoundException;

    /**
     * Gets the annotation of the training session indicated by the ID.
     *
     * @param user      The user requesting the training session annotation.
     * @param project   The ID of the project containing the training session object.
     * @param idOrLabel The ID or label of the training session within the specified project.
     *
     * @return The annotation of the specified training session.
     */
    @Nonnull
    String getSessionAnnotation(final UserI user, final String project, final String idOrLabel) throws NotFoundException;

    /**
     * Updates the notes on the training session indicated by the ID.
     *
     * @param user      The user updating the training session notes.
     * @param sessionId The ID of the training session.
     * @param notes     The notes to add to the training session.
     */
    void annotateSession(final UserI user, final String sessionId, final String notes) throws NotFoundException, DataFormatException;

    /**
     * Updates the notes on the training session indicated by the ID.
     *
     * @param user      The user updating the training session notes.
     * @param project   The ID of the project containing the training session object.
     * @param idOrLabel The ID or label of the training session within the specified project.
     * @param notes     The notes to add to the training session.
     */
    void annotateSession(final UserI user, final String project, final String idOrLabel, final String notes) throws NotFoundException, DataFormatException;

    /**
     * Returns all training sessions in the specified project that match the specified status or statuses.
     *
     * @param user     The user requesting the sessions.
     * @param project  The ID of the project containing the training session objects.
     * @param statuses The status or statuses to search for.
     *
     * @return A map, organized by status, of the IDs of all sessions in the project with the specified status or statuses.
     */
    @Nonnull
    Map<String, Map<String, Map<String, Object>>> findSessionsByStatus(final UserI user, final String project, final String... statuses) throws NotFoundException, InsufficientPrivilegesException;

    /**
     * Returns all training sessions in the specified project that match the specified status or statuses.
     *
     * @param user     The user requesting the sessions.
     * @param project  The ID of the project containing the training session objects.
     * @param statuses The status or statuses to search for.
     *
     * @return A map, organized by status, of the IDs of all sessions in the project with the specified status or statuses.
     */
    @Nonnull
    Map<String, Map<String, Map<String, Object>>> findSessionsByStatus(final UserI user, final String project, final List<String> statuses) throws NotFoundException, InsufficientPrivilegesException;

    /**
     * Creates a new training session.
     *
     * @param user    The user creating the training session.
     * @param session The training session object to be created.
     *
     * @return The newly created training session object.
     */
    @SuppressWarnings("unchecked")
    @Nonnull
    MlTrainsession createTrainingSession(final UserI user, final MlTrainsession session, final Consumer<MlTrainsession>... presaves) throws ResourceAlreadyExistsException, DataFormatException;

    /**
     * Creates a new training session.
     *
     * @param user          The user creating the training session.
     * @param processingId  The ID for the processing entry to handle the training session.
     * @param label         The label for the training session.
     * @param model         The model to be trained.
     * @param configuration The training configuration to use.
     * @param collection    The dataset to use.
     * @param parameters    The parameters for the training session.
     *
     * @return The newly created training session object.
     */
    @SuppressWarnings("unchecked")
    @Nonnull
    MlTrainsession createTrainingSession(final UserI user, final String processingId, final String label, final MlModelI model, final MlTrainconfigI configuration, final SetsCollectionI collection, final Map<String, String> parameters, final Consumer<MlTrainsession>... presaves) throws ResourceAlreadyExistsException, DataFormatException;

    /**
     * Gets the project, ID, and label of all training sessions accessible to the user.
     *
     * @param user The user requesting the sessions.
     *
     * @return The project, ID, and label of all training sessions accessible to the user.
     */
    @Nonnull
    Map<String, Map<String, String>> retrieveTrainingSessions(final UserI user);

    /**
     * Gets the IDs and labels of the training sessions associated with the indicated project.
     *
     * @param user    The user requesting the sessions.
     * @param project The ID of the project for which you want to retrieve the training sessions.
     *
     * @return The IDs and labels for the training session for the indicated project.
     */
    @Nonnull
    Map<String, Map<String, Object>> retrieveTrainingSessions(final UserI user, final String project) throws NotFoundException, InsufficientPrivilegesException;

    /**
     * Retrieves an existing training session.
     *
     * @param user      The user requesting the training session.
     * @param sessionId The ID of the training session object to be retrieved.
     *
     * @return The requested training session object.
     *
     * @throws NotFoundException When no training session exists with the specified ID.
     */
    @Nonnull
    MlTrainsession retrieveTrainingSession(final UserI user, final String sessionId) throws NotFoundException;

    /**
     * Retrieves an existing training session.
     *
     * @param user      The user requesting the training session.
     * @param project   The ID of the project containing the training session object to be retrieved.
     * @param idOrLabel The ID or label of the training session within the specified project.
     *
     * @return The requested training session object.
     *
     * @throws NotFoundException When no training session exists with the specified ID or label in the project.
     */
    @Nonnull
    MlTrainsession retrieveTrainingSession(final UserI user, final String project, final String idOrLabel) throws NotFoundException;

    /**
     * Updates the submitted training session in the indicated project.
     *
     * @param user    The user requesting the configuration.
     * @param session The session to be updated.
     *
     * @return The updated training session.
     */
    @Nonnull
    MlTrainsession updateTrainingSession(final UserI user, final MlTrainsession session) throws DataFormatException, NotFoundException;

    /**
     * Updates the submitted training session in the indicated project, including rendering and saving the training configuration
     * resources associated with the session.
     *
     * @param user       The user requesting the configuration.
     * @param session    The session to be updated.
     * @param parameters The parameters for updating training configuration resources.
     *
     * @return The updated training session.
     */
    @Nonnull
    MlTrainsession updateTrainingSession(final UserI user, final MlTrainsession session, final Map<String, String> parameters) throws DataFormatException, NotFoundException;

    /**
     * Deletes an existing training session.
     *
     * @param user      The user requesting the training session deletion.
     * @param sessionId The ID of the training session object to be deleted.
     *
     * @throws NotFoundException When no training session exists with the specified ID.
     */
    void deleteTrainingSession(final UserI user, final String sessionId) throws NotFoundException;

    /**
     * Deletes an existing training session.
     *
     * @param user      The user requesting the training session deletion.
     * @param project   The ID of the project containing the training session object to be deleted.
     * @param idOrLabel The ID or label of the training session within the specified project.
     *
     * @throws NotFoundException When no training session exists with the specified ID or label in the project.
     */
    void deleteTrainingSession(final UserI user, final String project, final String idOrLabel) throws NotFoundException;
}
