/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.converters.TrainsessionSerializer
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.converters;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xdat.om.MlTrainsession;
import org.nrg.xnatx.plugins.datasets.converters.DatasetSerializer;

@Slf4j
public class TrainsessionSerializer extends DatasetSerializer<MlTrainsession> {
    public TrainsessionSerializer() {
        super(MlTrainsession.class);
    }

    @Override
    public void serialize(final MlTrainsession trainSession, final JsonGenerator generator, final SerializerProvider serializer) throws IOException {
        generator.writeStartObject();
        writeNonBlankField(generator, "id", trainSession.getId());
        writeNonBlankField(generator, "label", trainSession.getLabel());
        writeNonBlankField(generator, "description", trainSession.getDescription());
        writeNonBlankField(generator, "project", trainSession.getProject());
        writeNonBlankField(generator, "trainConfigId", trainSession.getTrainconfigId());
        writeNonBlankField(generator, "modelId", trainSession.getModelId());
        writeNonBlankField(generator, "collectionId", trainSession.getCollectionId());
        writeNonBlankField(generator, "parameters", trainSession.getParameters());
        writeNonNullField(generator, "launchTime", trainSession.getLaunchtime());
        writeNonNullField(generator, "completionTime", trainSession.getCompletiontime());
        writeNonBlankField(generator, "notes", trainSession.getNotes());
        writeNonBlankField(generator, "processingId", trainSession.getProcessingId());
        writeNonBlankField(generator, "status", trainSession.getStatus());
        writeMetadata(generator, trainSession);
        generator.writeEndObject();
    }
}
