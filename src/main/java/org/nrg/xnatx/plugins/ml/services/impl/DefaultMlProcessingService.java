/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.services.impl.DefaultMlProcessingService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.services.impl;

import static java.util.stream.Collectors.*;
import static lombok.AccessLevel.PRIVATE;
import static org.nrg.xdat.om.base.BaseMlTrainsession.STATUS_IN_PROGRESS;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.nrg.framework.services.SerializerService;
import org.nrg.xapi.exceptions.DataFormatException;
import org.nrg.xapi.exceptions.InsufficientPrivilegesException;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.exceptions.ResourceAlreadyExistsException;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.MlModelI;
import org.nrg.xdat.model.MlTrainconfigI;
import org.nrg.xdat.model.SetsCollectionI;
import org.nrg.xdat.om.MlTrainconfig;
import org.nrg.xdat.om.MlTrainsession;
import org.nrg.xdat.om.SetsCollection;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.security.SecurityManager;
import org.nrg.xdat.security.helpers.Permissions;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.services.messaging.processing.ProcessingOperationRequest;
import org.nrg.xnat.utils.WorkflowUtils;
import org.nrg.xnatx.plugins.ml.exceptions.XnatMlException;
import org.nrg.xnatx.plugins.ml.models.LaunchTrainingRequestData;
import org.nrg.xnatx.plugins.ml.preferences.MlResources;
import org.nrg.xnatx.plugins.ml.services.MlConfigurationService;
import org.nrg.xnatx.plugins.ml.services.MlModelService;
import org.nrg.xnatx.plugins.ml.services.MlProcessingService;
import org.nrg.xnatx.plugins.datasets.services.DatasetCollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

@Service
@Getter(PRIVATE)
@Accessors(prefix = "_")
@Slf4j
public class DefaultMlProcessingService extends AbstractManagementService<MlTrainsession> implements MlProcessingService {
    @Autowired
    public DefaultMlProcessingService(final MlModelService models, final MlConfigurationService configurations, final DatasetCollectionService collections, final NamedParameterJdbcTemplate template, final SerializerService serializer, final MlResources resources) {
        super(template, resources);
        _models = models;
        _configurations = configurations;
        _collections = collections;
        _serializer = serializer;
        _nullMessage = getMessage("processing.messages.null");
    }

    public static String generateTrainingSessionLabel(final MlModelI model, final MlTrainconfigI configuration, final Date launchTime) {
        return model.getLabel() + "-" + configuration.getLabel() + "-" + launchTime.getTime();
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlTrainsession launchTrainingSession(final UserI user, final LaunchTrainingRequestData launchModel) throws NotFoundException, ResourceAlreadyExistsException, DataFormatException {
        return launchTrainingSession(user,
                                     launchModel.getProcessingId(),
                                     launchModel.getLabel(),
                                     getModels().retrieveMlModel(user, launchModel.getModelId()),
                                     getConfigurations().retrieveTrainingConfiguration(user, launchModel.getConfigurationId()),
                                     getCollections().findById(user, launchModel.getCollectionId()),
                                     launchModel.getParameters());
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlTrainsession launchTrainingSession(final UserI user, final String processingId, final String label, final MlModelI model, final MlTrainconfigI configuration, final SetsCollection collection, final Map<String, String> submitted) throws ResourceAlreadyExistsException, DataFormatException {
        final Map<String, String> parameters = ObjectUtils.defaultIfNull(submitted, Collections.emptyMap());
        return queueTrainingSession(user, createTrainingSession(user, processingId, label, model, configuration, collection, parameters, session -> storeTrainingConfigurationResources(user, session, (MlTrainconfig) configuration, collection, parameters)));
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlTrainsession launchTrainingSession(final UserI user, final String processingId, final String label, final String modelId, final String configurationId, final String collectionId, final Map<String, String> parameters) throws NotFoundException, ResourceAlreadyExistsException {
        try {
            return launchTrainingSession(user, processingId, label, _models.retrieveMlModel(user, modelId), _configurations.retrieveTrainingConfiguration(user, configurationId), _collections.findById(user, collectionId), parameters);
        } catch (NotFoundException | DataFormatException e) {
            throw new NotFoundException(e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlTrainsession launchTrainingSession(final UserI user, final String processingId, final String modelId, final String configurationId, final String collectionId, final Map<String, String> parameters) throws NotFoundException, ResourceAlreadyExistsException {
        return launchTrainingSession(user, processingId, null, modelId, configurationId, collectionId, parameters);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MlTrainsession queueTrainingSession(final UserI user, final String sessionId) throws NotFoundException, DataFormatException {
        return queueTrainingSession(user, retrieveTrainingSession(user, sessionId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MlTrainsession queueTrainingSession(final UserI user, final String project, final String idOrLabel) throws NotFoundException, DataFormatException {
        return queueTrainingSession(user, retrieveTrainingSession(user, resolveIdOrLabelInProject(project, idOrLabel)));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MlTrainsession queueTrainingSession(final UserI user, final MlTrainsession session) throws DataFormatException {
        saveXnatMlObject(user, session);
        final String sessionId = session.getId();

        final Map<String, String> parameterMap = session.getParameterMap();
        parameterMap.put("session", sessionId);
        parameterMap.put("model", session.getModelId());
        parameterMap.put("project", session.getProject());

        final LaunchTrainingRequestData data = LaunchTrainingRequestData.builder()
                                                                        .processingId(session.getProcessingId())
                                                                        .label(session.getLabel())
                                                                        .sessionId(sessionId)
                                                                        .modelId(session.getModelId())
                                                                        .configurationId(session.getTrainconfigId())
                                                                        .collectionId(session.getCollectionId())
                                                                        .username(user.getUsername())
                                                                        .workflowId(session.getWorkflowId())
                                                                        .parameters(parameterMap)
                                                                        .build();

        XDAT.sendJmsRequest(ProcessingOperationRequest.wrap(data));
        return MlTrainsession.getMlTrainsessionsById(sessionId, user, false);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public String getSessionAnnotation(final UserI user, final String sessionId) throws NotFoundException {
        return getExperiment(user, sessionId).getNotes();
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public String getSessionAnnotation(final UserI user, final String project, final String idOrLabel) throws NotFoundException {
        return getSessionAnnotation(user, resolveIdOrLabelInProject(project, idOrLabel));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void annotateSession(final UserI user, final String sessionId, final String notes) throws NotFoundException, DataFormatException {
        final MlTrainsession session = getExperiment(user, sessionId);
        session.setNotes(notes);
        saveXnatMlObject(user, session);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void annotateSession(final UserI user, final String project, final String idOrLabel, final String notes)
            throws NotFoundException, DataFormatException {
        annotateSession(user, resolveIdOrLabelInProject(project, idOrLabel), notes);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Map<String, Map<String, Map<String, Object>>> findSessionsByStatus(final UserI user, final String project, final String... statuses) throws NotFoundException, InsufficientPrivilegesException {
        return findSessionsByStatus(user, project, Arrays.asList(statuses));
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Map<String, Map<String, Map<String, Object>>> findSessionsByStatus(final UserI user, final String project, final List<String> statuses) throws NotFoundException, InsufficientPrivilegesException {
        verifyUserProjectAccess(user, project);
        return getTemplate().queryForList(QUERY_SESSIONS_IN_PROJECT_BY_STATUS, new MapSqlParameterSource("project", project).addValue("statuses", statuses)).stream()
                            .collect(groupingBy(session -> (String) session.get("status"),
                                                mapping(session -> getSessionModel(user, (String) session.get("id")),
                                                        toMap(session -> (String) session.get("id"), Function.identity()))));
    }

    /**
     * {@inheritDoc}
     */
    @SafeVarargs
    @Nonnull
    @Override
    public final MlTrainsession createTrainingSession(final UserI user, final MlTrainsession session, final Consumer<MlTrainsession>... presaves) throws ResourceAlreadyExistsException, DataFormatException {
        final MlTrainsession created = createXnatMlObject(user, session);

        Arrays.stream(presaves).forEach(presave -> presave.accept(created));
        final XnatResourcecatalog catalog = created.createSessionResults(user);

        final MlTrainsession resourced = MlTrainsession.getMlTrainsessionsById(created.getId(), user, false);
        if (resourced == null) {
            throw new XnatMlException("Tried to save the ML data training session with ID " + created.getId() + " but trying to retrieve the saved object resulted in null");
        }

        // Add workflow to ML data session for status tracking
        try {
            PersistentWorkflowI workflow = WorkflowUtils.buildOpenWorkflow(user, session.getItem(),
                                                                           EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.PROCESS,
                                                                                                       "ML train", "Container launch", ""));
            workflow.setStatus(PersistentWorkflowUtils.QUEUED);
            WorkflowUtils.save(workflow, workflow.buildEvent());

            // save workflow ID to ML session
            resourced.setWorkflowId(workflow.getWorkflowId().toString());
        } catch (Exception e) {
            log.error("Unable to set workflow for ML train session {}", resourced.getId(), e);
        }

        log.info("Created new ML training session {} along with session resources in catalog {} and workflow {}",
                 created.getId(), catalog.getUri(), resourced.getWorkflowId());
        return saveXnatMlObject(user, resourced);
    }

    /**
     * {@inheritDoc}
     */
    @SafeVarargs
    @Nonnull
    @Override
    public final MlTrainsession createTrainingSession(final UserI user, final String processingId, final String label, final MlModelI model, final MlTrainconfigI configuration, final SetsCollectionI collection, final Map<String, String> parameters, final Consumer<MlTrainsession>... presaves) throws ResourceAlreadyExistsException, DataFormatException {
        final String project = model.getProject();
        if (Stream.of(project, configuration.getProject()).distinct().count() != 1) {
            throw new XnatMlException(getMessage("processing.messages.mismatchedProjects", project, configuration.getProject()));
        }

        final MlTrainsession session = new MlTrainsession(user);
        try {
            final Date launchTime = new Date();
            session.setProcessingId(processingId);
            session.setLaunchtime(launchTime);
            session.setLabel(StringUtils.isNotBlank(label) ? label : generateTrainingSessionLabel(model, configuration, launchTime));
            session.setProject(project);
            session.setModelId(model.getId());
            session.setTrainconfigId(configuration.getId());
            session.setCollectionId(collection.getId());
            session.setParameterMap(parameters);
            session.setStatus(STATUS_IN_PROGRESS);
        } catch (Exception e) {
            throw new XnatMlException(user, e, getMessage("processing.messages.error"));
        }

        return createTrainingSession(user, session, presaves);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Map<String, Map<String, String>> retrieveTrainingSessions(final UserI user) {
        return Permissions.getAllAccessibleExperimentsOfType(getTemplate(), user, MlTrainsession.SCHEMA_ELEMENT_NAME, SecurityManager.READ);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public Map<String, Map<String, Object>> retrieveTrainingSessions(final UserI user, final String project) throws NotFoundException, InsufficientPrivilegesException {
        final Set<String> sessionIds = getObjectsInProject(user, project).keySet();
        return getSessionModels(user, sessionIds);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlTrainsession retrieveTrainingSession(final UserI user, final String sessionId) throws NotFoundException {
        return getExperiment(user, sessionId);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlTrainsession retrieveTrainingSession(final UserI user, final String project, final String idOrLabel) throws NotFoundException {
        return getExperiment(user, project, idOrLabel);
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlTrainsession updateTrainingSession(final UserI user, final MlTrainsession session) throws DataFormatException, NotFoundException {
        return updateTrainingSession(user, session, Collections.emptyMap());
    }

    /**
     * {@inheritDoc}
     */
    @Nonnull
    @Override
    public MlTrainsession updateTrainingSession(final UserI user, final MlTrainsession session, final Map<String, String> parameters) throws DataFormatException, NotFoundException {
        final MlTrainconfig  configuration = _configurations.retrieveTrainingConfiguration(user, session.getTrainconfigId());
        final SetsCollection    collection    = _collections.findById(user, session.getCollectionId());
        final MlTrainsession updated       = saveXnatMlObject(user, session);
        if (parameters != null && !parameters.isEmpty()) {
            final XnatResourcecatalog catalog = storeTrainingConfigurationResources(user, session, configuration, collection, parameters);
            log.info("Updated the training session {}, including generating resources for the associated resource catalog {}", session.getId(), catalog.getUri());
        } else {
            log.info("Updated the training session {}, but did not create/update resources for any associated resource catalogs because no parameters were provided", session.getId());
        }
        return updated;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteTrainingSession(final UserI user, final String sessionId) throws NotFoundException {
        deleteXnatMlObject(user, sessionId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteTrainingSession(final UserI user, final String project, final String idOrLabel) throws NotFoundException {
        deleteXnatMlObject(user, project, idOrLabel);
    }

    @Nonnull
    private Map<String, Map<String, Object>> getSessionModels(final UserI user, final Set<String> sessionIds) {
        return sessionIds.stream().collect(toMap(Function.identity(), sessionId -> getSessionModel(user, sessionId)));
    }

    @Nonnull
    private Map<String, Object> getSessionModel(final UserI user, final String sessionId) {
        final Map<String, Object> attributes = new HashMap<>();
        try {
            final MlTrainsession session    = retrieveTrainingSession(user, sessionId);
            Date                    launchTime = (Date) session.getLaunchtime();
            attributes.put("id", session.getId());
            attributes.put("label", session.getLabel());
            attributes.put("launchTime", launchTime.getTime());
            attributes.put("modelId", session.getModelId());
            attributes.put("configId", session.getTrainconfigId());
            attributes.put("collectionId", session.getCollectionId());
            attributes.put("processingId", session.getProcessingId());
            attributes.put("parameters", session.getParameters());
            attributes.put("status", getMlTrainStatus(user, session));
            attributes.put("hasResults", Boolean.toString(session.getResources_resource().stream()
                                                                 .anyMatch(r -> "Results".equals(r.getLabel()) && r.getFileCount() != null && r.getFileCount() > 0)));
        } catch (NotFoundException e) {
            log.warn("Got not found for session ID {}", sessionId);
        }
        return attributes;
    }

    /**
     * Get the status for the ML train session If in progress, use the workflow table for live status.
     * Otherwise, use stored value. Update stored value if workflow is complete or failed.
     *
     * @param user    the user
     * @param session the session
     *
     * @return the status
     */
    private String getMlTrainStatus(final UserI user, MlTrainsession session) {
        if (STATUS_IN_PROGRESS.equals(session.getStatus())) {
            Calendar launched = Calendar.getInstance();
            launched.setTime((Date) session.getLaunchtime());
            Calendar oneHourAgo = Calendar.getInstance();
            oneHourAgo.add(Calendar.HOUR, -1);
            //Set the default status to failed if session launched over an hour ago and no workflow associated with it
            String              status     = launched.before(oneHourAgo) ? PersistentWorkflowUtils.FAILED : PersistentWorkflowUtils.QUEUED;
            String              workflowId = session.getWorkflowId();
            PersistentWorkflowI workflow;
            if (StringUtils.isNotBlank(workflowId) &&
                (workflow = WorkflowUtils.getUniqueWorkflow(user, workflowId)) != null) {
                status = workflow.getStatus();
                if (status.contains(PersistentWorkflowUtils.FAILED) || status.equals(PersistentWorkflowUtils.COMPLETE)) {
                    session.setStatus(status);
                    try {
                        saveXnatMlObject(user, session);
                    } catch (DataFormatException e) {
                        log.warn("Unable to save updated status for ML session {}", session.getId(), e);
                    }
                }
            }
            return status;
        } else {
            return session.getStatus();
        }
    }

    @NotNull
    private XnatResourcecatalog storeTrainingConfigurationResources(final UserI user, final MlTrainsession session, final MlTrainconfig configuration, final SetsCollection collection, final Map<String, String> parameters) {
        final JsonNode                       node             = _configurations.renderTrainingConfiguration(user, configuration, collection, parameters);
        final JsonNode                       configTrain      = node.get("configuration");
        final JsonNode                       dataset          = node.get("dataset");
        final Map<String, InputStreamSource> resources        = new HashMap<>();
        final String                         configTrainValue = configTrain.toString();
        final String                         datasetValue     = dataset.toString();
        resources.put("config_train.json", new ByteArrayResource(configTrainValue.getBytes()));
        resources.put("dataset.json", new ByteArrayResource(datasetValue.getBytes()));
        if (log.isDebugEnabled()) {
            log.debug("Rendered configuration {} to config_train.json containing {} characters and dataset.json containing {} characters\n\nconfig_train.json:\n\n{}\n\ndataset.json:\n\n{}", session.getTrainconfigId(), configTrainValue.length(), datasetValue.length(), configTrainValue, datasetValue);
        } else {
            log.info("Rendered configuration {} to config_train.json containing {} characters and dataset.json containing {} characters", session.getTrainconfigId(), configTrainValue.length(), datasetValue.length());
        }
        return session.setSessionData(user, resources);
    }

    //language=Postgresql
    private static final String QUERY_SESSIONS_IN_PROJECT_BY_STATUS = "SELECT "
                                                                      + "    status, "
                                                                      + "    id, "
                                                                      + "    coalesce(s.label, e.label) AS label "
                                                                      + "FROM "
                                                                      + "    xnat_experimentdata e "
                                                                      + "    LEFT JOIN xnat_experimentdata_share s ON e.id = s.sharing_share_xnat_experimentda_id AND s.project = :project "
                                                                      + "    LEFT JOIN xdat_meta_element m ON e.extension = m.xdat_meta_element_id "
                                                                      + "WHERE "
                                                                      + "    m.element_name = '" + MlTrainsession.SCHEMA_ELEMENT_NAME + "' AND "
                                                                      + "    (e.project = :project OR s.project = :project) AND "
                                                                      + "    status IN (:statuses)";

    private final MlModelService           _models;
    private final MlConfigurationService   _configurations;
    private final DatasetCollectionService _collections;
    private final SerializerService         _serializer;
    private final String                    _nullMessage;
}
