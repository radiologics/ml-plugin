/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.rest.MlXapiRestControllerAdvice
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.rest;

import static org.apache.commons.lang3.StringUtils.defaultIfBlank;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xdat.XDAT;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.ml.exceptions.XnatMlException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(annotations = XapiRestController.class)
@Slf4j
public class MlXapiRestControllerAdvice {
    @ExceptionHandler(XnatMlException.class)
    public ResponseEntity<?> handleXnatMlException(final HttpServletRequest request, final XnatMlException exception) {
        final String     message     = exception.getMessage();
        final String     statusValue = (String) exception.getParameter("status");
        final HttpStatus status      = StringUtils.isNotBlank(statusValue) ? HttpStatus.valueOf(statusValue) : DEFAULT_ERROR_STATUS;

        // Log 500s as errors, other statuses can just be logged as info messages.
        final UserI  userDetails = XDAT.getUserDetails();
        final String username    = userDetails != null ? userDetails.getUsername() : "unauthenticated user";
        final String requestUri  = request.getServletPath() + request.getPathInfo();

        if (status == INTERNAL_SERVER_ERROR) {
            log.error("HTTP status 500: Request by user {} to URL {} caused an internal server error", username, requestUri, exception);
        } else {
            log.info("HTTP status {}: Request by user {} to URL {} caused an exception of type {}{}", status, username, requestUri, exception.getClass().getName(), defaultIfBlank(message, ""));
        }

        final ResponseEntity.BodyBuilder builder = ResponseEntity.status(status);
        return isBlank(message) ? builder.contentLength(0).build() : builder.contentType(MediaType.TEXT_PLAIN).contentLength(message.length()).body(message);
    }

    private static final HttpStatus DEFAULT_ERROR_STATUS = INTERNAL_SERVER_ERROR;
}
