/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.converters.TrainsessionDeserializer
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.converters;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xdat.om.MlTrainsession;
import org.nrg.xnatx.plugins.datasets.converters.DatasetDeserializer;

@Slf4j
public class TrainsessionDeserializer extends DatasetDeserializer<MlTrainsession> {
    public TrainsessionDeserializer() {
        super(MlTrainsession.class);
    }

    @Override
    public MlTrainsession deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {
        if (parser.getCurrentToken() != JsonToken.START_OBJECT) {
            throw new IOException("invalid start marker");
        }

        final MlTrainsession session = new MlTrainsession();
        while (parser.nextToken() != JsonToken.END_OBJECT) {
            final String field = parser.getCurrentName();
            parser.nextToken();  //move to next token in string
            switch (field) {
                case "id":
                    session.setId(parser.getText());
                    break;
                case "project":
                    session.setProject(parser.getText());
                    break;
                case "label":
                    session.setLabel(parser.getText());
                    break;
                case "trainConfigId":
                    session.setTrainconfigId(parser.getText());
                    break;
                case "modelId":
                    session.setModelId(parser.getText());
                    break;
                case "collectionId":
                    session.setCollectionId(parser.getText());
                    break;
                case "parameters":
                    session.setParameters(parser.getText());
                    break;
                case "notes":
                    session.setNotes(parser.getText());
                    break;
                case "processingId":
                    session.setProcessingId(parser.getText());
                    break;
                case "status":
                    session.setStatus(parser.getText());
                    break;
            }
        }
        return session;
    }
}
