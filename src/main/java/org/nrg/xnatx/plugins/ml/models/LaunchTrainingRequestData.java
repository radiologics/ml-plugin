/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.models.LaunchTrainingRequestData
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Map;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import org.nrg.xnat.services.messaging.processing.ProcessingOperationRequestData;

import javax.annotation.Nullable;

@ApiModel(description = "Provides a container for the properties required to launch an ML training session.")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(prefix = "_")
public class LaunchTrainingRequestData extends ProcessingOperationRequestData implements Serializable {
    @Builder
    public LaunchTrainingRequestData(final String label, final String sessionId, final String modelId, final String configurationId, final String collectionId, final String processingId, final String username, final String workflowId, final Map<String, String> parameters) {
        super(processingId, username, parameters, workflowId);
        setLabel(label);
        setSessionId(sessionId);
        setModelId(modelId);
        setConfigurationId(configurationId);
        setCollectionId(collectionId);
        setWorkflowId(workflowId);
    }

    @ApiModelProperty("Provides a label for the training session instance. If no value is provided, a label is generated automatically.")
    @NonNull
    private String _label;

    @ApiModelProperty("Indicates the ID for the training session instance. This can be left blank when creating a new training session instance, and can provide the ID value when queuing a training session for launch.")
    @NonNull
    private String _sessionId;

    @ApiModelProperty(value = "Indicates the model to use for the training session.", required = true)
    @NonNull
    private String _modelId;

    @ApiModelProperty(value = "Indicates the training configuration to use for the training session.", required = true)
    @NonNull
    private String _configurationId;

    @ApiModelProperty(value = "Indicates the dataset to use for the training session.", required = true)
    @NonNull
    private String _collectionId;

    @ApiModelProperty(value = "Indicates the workflow to update for training session progress.")
    @Nullable
    private String _workflowId;
}
