/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.utils.ValueConvertingParameterMap
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.utils;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

public class ValueConvertingParameterMap extends HashMap<String, String> {
    public ValueConvertingParameterMap() {
        super();
    }

    public ValueConvertingParameterMap(final Map<String, String> parameters) {
        super(parameters);
    }

    public Object getConvertedValue(final String key) {
        return convertValue(get(key));
    }

    /**
     * Provided to detect object type of a value and convert to an instance of the appropriate class.
     * For example, a string like "3" is converted to an integer, "2.1" or "1e-4" is converted to a
     * double, "true" is converted to a boolean, and anything else is returned back as a string.
     *
     * @param value The value to be converted.
     *
     * @return An object containing the value in an instance of the appropriate class.
     */
    private Object convertValue(final String value) {
        if (StringUtils.isBlank(value)) {
            return "";
        }
        if (isBoolean(value)) {
            return BooleanUtils.toBooleanObject(value);
        }
        if (isInteger(value)) {
            return NumberUtils.createInteger(value);
        }
        if (isDecimal(value)) {
            return NumberUtils.createDouble(value);
        }
        return value.trim();
    }

    private boolean isDecimal(final String value) {
        return value.matches(IS_DECIMAL) || value.matches(IS_EXPONENTIAL);
    }

    private boolean isInteger(final String value) {
        return value.matches(IS_INTEGER);
    }

    private boolean isBoolean(final String value) {
        return value.matches(IS_BOOLEAN);
    }

    private static final String IS_EXPONENTIAL = "^[-+]?[0-9]+[Ee][+-][0-9]+$";
    private static final String IS_DECIMAL     = "^[-+]?[0-9]*\\.[0-9]+$";
    private static final String IS_INTEGER     = "^[-+]?[0-9]+$";
    private static final String IS_BOOLEAN     = "^(true|TRUE|false|FALSE)$";
}
