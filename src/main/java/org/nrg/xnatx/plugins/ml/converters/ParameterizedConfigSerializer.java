/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.converters.ParameterizedConfigSerializer
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.converters;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xdat.om.MlParameterizedconfig;
import org.nrg.xnatx.plugins.datasets.converters.DatasetSerializer;

@Slf4j
public class ParameterizedConfigSerializer extends DatasetSerializer<MlParameterizedconfig> {
    public ParameterizedConfigSerializer() {
        super(MlParameterizedconfig.class);
    }

    @Override
    public void serialize(final MlParameterizedconfig configuration, final JsonGenerator generator, final SerializerProvider serializer) throws IOException {
        generator.writeStartObject();

        writeNonBlankJson(generator, "template", configuration.getTemplate());

        final String parameterizable = configuration.getParameterizable();
        if (StringUtils.startsWithAny(parameterizable, "[", "{")) {
            log.info("Writing parameterizable as raw JSON object");
            writeNonBlankJson(generator, "parameterizable", parameterizable);
        } else {
            log.info("Writing parameterizable as string field");
            writeNonBlankField(generator, "parameterizable", parameterizable);
        }

        generator.writeEndObject();
    }
}
