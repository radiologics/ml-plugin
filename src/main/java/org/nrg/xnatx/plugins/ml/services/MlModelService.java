/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.services.MlModelService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.services;

import java.util.Map;
import javax.annotation.Nonnull;
import org.nrg.xapi.exceptions.InsufficientPrivilegesException;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xdat.om.MlModel;
import org.nrg.xft.security.UserI;
import org.springframework.core.io.InputStreamSource;

public interface MlModelService {
    /**
     * Creates the submitted model.
     *
     * @param user  The user requesting the model.
     * @param model The model to be created.
     *
     * @return The newly created model.
     */
    @Nonnull
    MlModel createMlModel(final UserI user, final MlModel model);

    /**
     * Creates the submitted model from JSON.
     *
     * @param user    The user requesting the model.
     * @param project The ID of the model's project.
     * @param label   The model label.
     *
     * @return The newly created model.
     */
    @Nonnull
    MlModel createMlModel(final UserI user, final String project, final String label);

    /**
     * Creates the submitted model.
     *
     * @param user       The user requesting the model.
     * @param model      The model to be created.
     * @param references One or more readable sources indicating the data to add to the model.
     *
     * @return The newly created model.
     */
    @Nonnull
    MlModel createMlModel(final UserI user, final MlModel model, final Map<String, ? extends InputStreamSource> references);

    /**
     * Creates the submitted model from JSON.
     *
     * @param user       The user requesting the model.
     * @param project    The ID of the model's project.
     * @param label      The model label.
     * @param references One or more readable sources indicating the data to add to the model.
     *
     * @return The newly created model.
     */
    @Nonnull
    MlModel createMlModel(final UserI user, final String project, final String label, final Map<String, ? extends InputStreamSource> references);

    /**
     * Gets the IDs and labels of the models associated with the indicated project.
     *
     * @param user    The user requesting the models.
     * @param project The ID of the project for which you want to retrieve the models.
     *
     * @return The IDs and labels for the model for the indicated project.
     */
    @Nonnull
    Map<String, String> retrieveMlModels(final UserI user, final String project) throws NotFoundException, InsufficientPrivilegesException;

    /**
     * Gets the model associated with the indicated project with the specified ID or
     * label within the project.
     *
     * @param user    The user requesting the model.
     * @param modelId The ID of the model object.
     *
     * @return The model with the indicated ID, null if not found.
     */
    @Nonnull
    MlModel retrieveMlModel(final UserI user, final String modelId) throws NotFoundException;

    /**
     * Gets the model associated with the indicated project with the specified ID or
     * label within the project.
     *
     * @param user      The user requesting the model.
     * @param project   The ID of the project for which you want to retrieve the model.
     * @param idOrLabel The ID or name within the project of the model object.
     *
     * @return The model for the indicated project and type, null if not found.
     */
    @Nonnull
    MlModel retrieveMlModel(final UserI user, final String project, final String idOrLabel) throws NotFoundException;

    /**
     * Updates the submitted model in the indicated project.
     *
     * @param user  The user requesting the model.
     * @param model The model to be updated.
     *
     * @return The updated model.
     */
    @Nonnull
    MlModel updateMlModel(final UserI user, final MlModel model) throws NotFoundException;

    /**
     * Updates the submitted model.
     *
     * @param user       The user requesting the model.
     * @param model      The model to be updated.
     * @param references One or more readable sources indicating the data to add to the model.
     *
     * @return The updated model.
     */
    @Nonnull
    MlModel updateMlModel(final UserI user, final MlModel model, final Map<String, ? extends InputStreamSource> references);

    /**
     * Updates the submitted model from JSON.
     *
     * @param user       The user requesting the model.
     * @param modelId    The ID of the model object.
     * @param references One or more readable sources indicating the data to add to the model.
     *
     * @return The updated model.
     */
    @Nonnull
    MlModel updateMlModel(final UserI user, final String modelId, final Map<String, ? extends InputStreamSource> references) throws NotFoundException;

    /**
     * Updates the submitted model from JSON.
     *
     * @param user       The user requesting the model.
     * @param project    The ID of the model's project.
     * @param label      The model label.
     * @param references One or more readable sources indicating the data to add to the model.
     *
     * @return The updated model.
     */
    @Nonnull
    MlModel updateMlModel(final UserI user, final String project, final String label, final Map<String, ? extends InputStreamSource> references) throws NotFoundException;

    /**
     * Deletes the indicated model.
     *
     * @param user    The user requesting the model.
     * @param modelId The ID of the model object.
     */
    void deleteMlModel(final UserI user, final String modelId) throws NotFoundException;

    /**
     * Deletes the indicated model in the indicated project.
     *
     * @param user      The user requesting the model.
     * @param project   The ID of the project for which you want to retrieve the model.
     * @param idOrLabel The ID or name within the project of the model object.
     */
    void deleteMlModel(final UserI user, final String project, final String idOrLabel) throws NotFoundException;
}
