/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.converters.ModelSerializer
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.converters;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xdat.om.MlModel;
import org.nrg.xnatx.plugins.datasets.converters.DatasetSerializer;

import java.io.IOException;

@Slf4j
public class ModelSerializer extends DatasetSerializer<MlModel> {
    public ModelSerializer() {
        super(MlModel.class);
    }

    @Override
    public void serialize(final MlModel model, final JsonGenerator generator, final SerializerProvider serializer) throws IOException {
        generator.writeStartObject();
        writeNonBlankField(generator, "id", model.getId());
        writeNonBlankField(generator, "label", model.getLabel());
        writeNonBlankField(generator, "name", model.getName());
        writeNonBlankField(generator, "modelVersion", model.getModelversion());
        writeNonBlankField(generator, "description", model.getDescription());
        writeNonBlankField(generator, "project", model.getProject());
        writeNonBlankField(generator, "format", model.getFormat());
        writeNonBlankField(generator, "status", model.getStatus());
        writeNonBlankField(generator, "source", model.getSource());
        writeNonBlankField(generator, "type", model.getType());
        writeMetadata(generator, model);
        generator.writeEndObject();
    }
}
