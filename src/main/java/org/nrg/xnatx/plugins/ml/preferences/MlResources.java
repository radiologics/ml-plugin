/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.preferences.XnatMlResources
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.preferences;

import java.util.Locale;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Service;

@Service
public class MlResources {
    public MlResources() {
        _resources = new ResourceBundleMessageSource();
        _resources.setBasename(BUNDLE_NAME);
    }

    public String getMessage(final String key, final Object... params) {
        try {
            return _resources.getMessage(key, params, Locale.getDefault());
        } catch (NoSuchMessageException e) {
            return '!' + key + '!';
        }
    }

    private static final String BUNDLE_NAME = "org.nrg.xnatx.plugins.ml.preferences.ml-messages";

    private final ResourceBundleMessageSource _resources;
}
