/*
 * ml-plugin: org.nrg.xnatx.plugins.ml.services.impl.AbstractManagementService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2021, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.ml.services.impl;

import static lombok.AccessLevel.PROTECTED;

import com.google.common.collect.ImmutableMap;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.nrg.action.ClientException;
import org.nrg.framework.generics.AbstractParameterizedWorker;
import org.nrg.xapi.exceptions.DataFormatException;
import org.nrg.xapi.exceptions.InsufficientPrivilegesException;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.exceptions.ResourceAlreadyExistsException;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.security.helpers.Permissions;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xnatx.plugins.ml.exceptions.XnatMlException;
import org.nrg.xnatx.plugins.ml.preferences.MlResources;
import org.nrg.xnatx.plugins.datasets.exceptions.DatasetDefinitionHandlingException;
import org.nrg.xnatx.plugins.datasets.exceptions.DatasetObjectException;
import org.restlet.data.Status;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.annotation.Nonnull;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

// TODO: This should be combined with the AbstractXftDatasetObjectService in the collections plugin and both moved into base XNAT.
@Getter(PROTECTED)
@Accessors(prefix = "_")
@Slf4j
public abstract class AbstractManagementService<T extends XnatExperimentdata> extends AbstractParameterizedWorker<T> {
    protected AbstractManagementService(final NamedParameterJdbcTemplate template, final MlResources resources) {
        _template = template;
        _resources = resources;
        _dataType = getParameterizedType();
        try {
            _factoryMethod = _dataType.getMethod("get" + _dataType.getSimpleName() + "sById", Object.class, UserI.class, Boolean.TYPE);
        } catch (NoSuchMethodException e) {
            throw new DatasetObjectException("Got an error trying to get the ItemI constructor for the data type: " + _dataType.getName(), e);
        }
        try {
            _xsiType = (String) _dataType.getField("SCHEMA_ELEMENT_NAME").get(null);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            throw new XnatMlException(e, "Error trying to get schema element name for class " + _dataType.getName());
        }

        final ImmutableMap<String, String> parameter = ImmutableMap.of(DATA_TYPE_PARAMETER, getXsiType());
        _queryObjectsOfTypeInProject = StringSubstitutor.replace(QUERY_OBJECTS_OF_TYPE_IN_PROJECT, parameter);
        _queryObjectById = StringSubstitutor.replace(QUERY_OBJECT_BY_ID_AND_DATA_TYPE, parameter);
        _queryObjectExistsById = "SELECT EXISTS(" + _queryObjectById + ")";
        _queryObjectInProjectByIdOrLabel = StringSubstitutor.replace(QUERY_OBJECT_IN_PROJECT_BY_ID_OR_LABEL_AND_DATA_TYPE, parameter);
    }

    @Nonnull
    protected T createXnatMlObject(final UserI user, final T xnatMlObject) throws ResourceAlreadyExistsException, DataFormatException {
        final String id = xnatMlObject.getId();
        if (StringUtils.isBlank(id)) {
            xnatMlObject.setId(getNewExperimentId());
        } else if (checkAnyObjectExistsById(id)) {
            throw new ResourceAlreadyExistsException(getXsiType(), id);
        }
        if (checkAnyObjectExistsByProjectAndIdOrLabel(xnatMlObject.getProject(), xnatMlObject.getLabel())) {
            throw new ResourceAlreadyExistsException(getXsiType(), xnatMlObject.getProject() + ":" + xnatMlObject.getLabel());
        }
        return saveXnatMlObject(user, xnatMlObject);
    }

    @Nonnull
    protected T saveXnatMlObject(final UserI user, final T xnatMlObject) throws DataFormatException {
        try {
            SaveItemHelper.authorizedSave(xnatMlObject, user, false, false, false, true, EventUtils.DEFAULT_EVENT(user, "Created new ML data object of type " + xnatMlObject.getXSIType() + " for project " + xnatMlObject.getProject()));
            return getExperiment(user, xnatMlObject.getId());
        } catch (XnatMlException | DataFormatException e) {
            throw e;
        } catch (Exception e) {
            throw new XnatMlException(user, e, "An error occurred trying to save a new ML data object of type " + xnatMlObject.getXSIType() + " in project " + xnatMlObject.getProject());
        }
    }

    protected void deleteXnatMlObject(final UserI user, final String project, final String idOrLabel) throws NotFoundException {
        deleteXnatMlObject(user, getExperiment(user, project, idOrLabel));
    }

    protected void deleteXnatMlObject(final UserI user, final String id) throws NotFoundException {
        deleteXnatMlObject(user, getExperiment(user, id));
    }

    protected void deleteXnatMlObject(final UserI user, final T experiment) {
        try {
            SaveItemHelper.authorizedDelete(experiment.getItem(), user, EventUtils.DEFAULT_EVENT(user, "Deleted ML data object " + experiment.getId()));
        } catch (Exception e) {
            throw new XnatMlException(user, e, getMessage("error.unknown", user.getUsername() + " deleting ML data object " + experiment.getId()));
        }

    }

    @Nonnull
    protected String getNewExperimentId() {
        try {
            return XnatExperimentdata.CreateNewID();
        } catch (Exception e) {
            throw new XnatMlException(e, "An error occurred trying to create a new experiment ID");
        }
    }

    @Nonnull
    protected T getExperiment(final UserI user, final String id) throws NotFoundException {
        if (!_template.queryForObject(QUERY_EXPT_ID_WITH_DATA_TYPE_EXISTS, new MapSqlParameterSource(EXPERIMENT_PARAMETER, id).addValue(DATA_TYPE_PARAMETER, _xsiType), Boolean.class)) {
            throw new NotFoundException("User " + user.getUsername() + " requested " + _xsiType + " experiment with ID " + id + ", but that doesn't exist.");
        }
        try {
            return _dataType.cast(_factoryMethod.invoke(null, id, user, true));
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new DatasetDefinitionHandlingException("An error occurred invoking the factory method " + _dataType.getName() + "." + _factoryMethod.getName() + "() for the class implementation of the schema element for XSI type " + _xsiType, e);
        }
    }

    @Nonnull
    protected T getExperiment(final UserI user, final String projectId, final String idOrLabel) throws NotFoundException {
        return getExperiment(user, resolveIdOrLabelInProject(projectId, idOrLabel));
    }

    @Nonnull
    protected T saveExperiment(final UserI user, final T experiment) {
        try {
            if (StringUtils.isBlank(experiment.getId())) {
                experiment.setId(getNewExperimentId());
            }
            final String id = experiment.getId();
            SaveItemHelper.authorizedSave(experiment, user, false, false, false, true, EventUtils.DEFAULT_EVENT(user, "Created new " + experiment.getXSIType() + " for project " + experiment.getProject()));
            return getExperiment(user, id);
        } catch (ClientException e) {
            final XnatMlException exception;
            if (e.getStatus() == Status.CLIENT_ERROR_CONFLICT) {
                exception = new XnatMlException(user, "The " + experiment.getXSIType() + " you're trying to save conflicts with an existing model definition: " + e.getMessage());
            } else {
                exception = new XnatMlException(user, e, "A client error occurred trying to save a new instance of " + experiment.getXSIType());
            }
            exception.setParameter("status", e.getStatus());
            throw exception;
        } catch (Exception e) {
            throw new XnatMlException(user, e, "An error occurred trying to save a new instance of " + experiment.getXSIType());
        }
    }

    protected Map<String, String> getObjectsInProject(final UserI user, final String project) throws NotFoundException, InsufficientPrivilegesException {
        verifyUserProjectAccess(user, project);
        return _template.queryForList(_queryObjectsOfTypeInProject, new MapSqlParameterSource(PROJECT_PARAMETER, project)).stream().collect(Collectors.toMap(config -> (String) config.get(ID_PARAMETER), config -> (String) config.get(LABEL_PARAMETER)));
    }

    protected void verifyUserProjectAccess(final UserI user, String project) throws NotFoundException, InsufficientPrivilegesException {
        verifyProjectExists(project);
        verifyUserCanReadProject(user, project);
    }

    protected void verifyProjectExists(final String project) throws NotFoundException {
        if (!Permissions.verifyProjectExists(getTemplate(), project)) {
            throw new NotFoundException("The requested project " + project + " does not exist");
        }
    }

    protected void verifyUserCanReadProject(final UserI user, final String project) throws InsufficientPrivilegesException {
        if (!Permissions.canReadProject(((JdbcTemplate) getTemplate().getJdbcOperations()), user, project)) {
            throw new InsufficientPrivilegesException("The user " + user.getUsername() + " does not have permission to read the requested project " + project);
        }
    }

    protected boolean checkAnyObjectExistsById(final String objectId) {
        return _template.queryForObject(QUERY_EXISTS_OBJECT_BY_ID, new MapSqlParameterSource("id", objectId), Boolean.class);
    }

    protected boolean checkAnyObjectExistsByProjectAndIdOrLabel(final @Nonnull String project, final @Nonnull String idOrLabel) {
        return _template.queryForObject(QUERY_EXISTS_OBJECT_IN_PROJECT_BY_ID_OR_LABEL, new MapSqlParameterSource(PROJECT_PARAMETER, project).addValue(ID_OR_LABEL_PARAMETER, idOrLabel), Boolean.class);
    }

    protected String resolveIdOrLabelInProject(final String project, final String idOrLabel) throws NotFoundException {
        if (StringUtils.isNotBlank(project)) {
            try {
                return _template.queryForObject(_queryObjectInProjectByIdOrLabel, new MapSqlParameterSource(PROJECT_PARAMETER, project).addValue(ID_OR_LABEL_PARAMETER, idOrLabel), String.class);
            } catch (EmptyResultDataAccessException e) {
                throw new NotFoundException(getNotFoundMessage(project, idOrLabel));
            }
        }
        if (checkAnyObjectExistsById(idOrLabel)) {
            return idOrLabel;
        }
        throw new NotFoundException(getNotFoundMessage(idOrLabel));
    }

    protected String getNotFoundMessage(final String project, final String idOrLabel) {
        return getMessage("object.not.found.by.projectAndLabel", getXsiType(), project, idOrLabel);
    }

    protected String getNotFoundMessage(final String objectId) {
        return getMessage("object.not.found.by.objectId", getXsiType(), objectId);
    }

    protected String getMessage(final String key, final Object... parameters) {
        return getResources().getMessage(key, parameters);
    }

    private static final String DATA_TYPE_PARAMETER                                  = "dataType";
    private static final String PROJECT_PARAMETER                                    = "project";
    private static final String ID_PARAMETER                                         = "id";
    private static final String ID_OR_LABEL_PARAMETER                                = "idOrLabel";
    private static final String EXPERIMENT_PARAMETER                                 = "experiment";
    private static final String QUERY_OBJECTS_OF_TYPE_IN_PROJECT                     = "SELECT "
                                                                                       + "    id, "
                                                                                       + "    coalesce(s.label, e.label) AS label "
                                                                                       + "FROM "
                                                                                       + "    xnat_experimentdata e "
                                                                                       + "    LEFT JOIN xnat_experimentdata_share s ON e.id = s.sharing_share_xnat_experimentda_id AND s.project = :project "
                                                                                       + "    LEFT JOIN xdat_meta_element m ON e.extension = m.xdat_meta_element_id "
                                                                                       + "WHERE "
                                                                                       + "    m.element_name = '${dataType}' AND "
                                                                                       + "    (e.project = :project OR s.project = :project)";
    private static final String QUERY_OBJECT_BY_ID                                   = "SELECT "
                                                                                       + "    e.id, "
                                                                                       + "    e.label, "
                                                                                       + "    e.project "
                                                                                       + "FROM "
                                                                                       + "    xnat_experimentdata e "
                                                                                       + "    LEFT JOIN xdat_meta_element m ON e.extension = m.xdat_meta_element_id "
                                                                                       + "WHERE "
                                                                                       + "    e.id = :id";
    private static final String QUERY_OBJECT_BY_ID_AND_DATA_TYPE                     = QUERY_OBJECT_BY_ID + " AND m.element_name = '${dataType}'";
    private static final String QUERY_OBJECT_IN_PROJECT_BY_ID_OR_LABEL               = "SELECT "
                                                                                       + "    e.id AS id "
                                                                                       + "FROM "
                                                                                       + "    xnat_experimentdata e "
                                                                                       + "    LEFT JOIN xnat_experimentdata_share s ON e.id = s.sharing_share_xnat_experimentda_id AND s.project = :project "
                                                                                       + "    LEFT JOIN xdat_meta_element m ON e.extension = m.xdat_meta_element_id "
                                                                                       + "WHERE "
                                                                                       + "    (((e.id = :idOrLabel OR e.label = :idOrLabel) AND e.project = :project) OR "
                                                                                       + "     ((s.sharing_share_xnat_experimentda_id = :idOrLabel OR s.label = :idOrLabel) AND s.project = :project))";
    private static final String QUERY_OBJECT_IN_PROJECT_BY_ID_OR_LABEL_AND_DATA_TYPE = QUERY_OBJECT_IN_PROJECT_BY_ID_OR_LABEL + " AND m.element_name = '${dataType}'";
    private static final String QUERY_IMAGE_SESSION_BY_ID                            = "SELECT "
                                                                                       + "   id "
                                                                                       + "FROM "
                                                                                       + "    xnat_imagesessiondata "
                                                                                       + "WHERE "
                                                                                       + "    id = :id";
    private static final String QUERY_EXISTS_IMAGE_SESSION_BY_ID                     = "SELECT EXISTS(SELECT "
                                                                                       + "   id "
                                                                                       + "FROM "
                                                                                       + "    xnat_imagesessiondata "
                                                                                       + "WHERE "
                                                                                       + "    id = :id)";
    private static final String QUERY_EXISTS                                         = "SELECT EXISTS(%s)";
    private static final String QUERY_EXPT_ID_WITH_DATA_TYPE                         = "SELECT id FROM xnat_experimentdata e LEFT JOIN xdat_meta_element m ON e.extension = m.xdat_meta_element_id WHERE e.id = :" + EXPERIMENT_PARAMETER + " AND m.element_name = :" + DATA_TYPE_PARAMETER;
    private static final String QUERY_EXPT_ID_WITH_DATA_TYPE_EXISTS                  = String.format(QUERY_EXISTS, QUERY_EXPT_ID_WITH_DATA_TYPE);
    private static final String QUERY_EXISTS_OBJECT_BY_ID                            = String.format(QUERY_EXISTS, QUERY_OBJECT_BY_ID);
    private static final String QUERY_EXISTS_OBJECT_IN_PROJECT_BY_ID_OR_LABEL        = String.format(QUERY_EXISTS, QUERY_OBJECT_IN_PROJECT_BY_ID_OR_LABEL);

    private static final Pattern DATA_TYPE_NAME_PATTERN = Pattern.compile("^(?<start>[^A-Z]+)(?<middle>[A-Z])(?<end>[^A-Z]*)$");
    private static final String  LABEL_PARAMETER        = "label";

    private final NamedParameterJdbcTemplate _template;
    private final MlResources                _resources;
    private final String                     _xsiType;
    private final Class<? extends T>         _dataType;
    private final Method                     _factoryMethod;
    private final String                     _queryObjectsOfTypeInProject;
    private final String                     _queryObjectById;
    private final String                     _queryObjectExistsById;
    private final String                     _queryObjectInProjectByIdOrLabel;
}
